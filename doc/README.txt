Prehistoria est un "Survival RPG" développé par Aurélien Bidon et Emeric de Peretti dans le cadre de leur études en Informatique à l'IUT de Villetaneuse. 

Le jeu prend place dans un univers préhistorique dans lequel le joueur doit lutter pour sa survie face a une nature meurtrière. Le craft et l'évolution prennent une place importante dans le jeu puisque la survie de l'Homme a, à l'époque, reposé sur la découverte de technologies aujourd'hui évidentes telles que la fabrication d'outils/armes, la confection de vetements.

Dès le jeu lancé, vous vous retrouvez sur un menu et plusieurs possiblités s'offrent à vous :

	- Nouvelle partie : Crée une nouvelle partie et lance le jeu.
	- Charger une partie : Utilise une sauvegarde (s'il en existe une) afin de reprendre le jeu là où il était.
	- Options : Ouvre le menu option permettant de changer la langue.
	- Quitter : Ferme le jeu.

Nouvelle Partie :

Lorsque vous choisissez nouvelle partie, un menu s'ouvre vous demandant votre Pseudo ainsi que votre classe afin de paramétrer le jeu.
Une fois ceci terminé, le jeu se lance, vous vous retrouver sur un nouveau menu décomposé en plusieurs parties :

- En haut à gauche se trouve les informations concernant la carte sur laquelle vous vous trouvez actuellement.
- Au centre se trouve la carte sur laquelle vous vous trouvez, plusieurs éléments sont affichés :
	- Les cases de couleur correspondent à différents types de sol :
		- Les cases bleus representent l'eau.
		- Les cases jaunes representent le sable.
		- Les cases vertes représentent l'herbe.

	- Les cases sur lesquelles sont affichés des charactères textuels :
		- La case où est inscrit "Me" vous représente, il est écrit en bleu sur fond blanc.
		- Les cases où sont inscrit "##" représentent les murs, vous ne pouvez bien évidemment pas vous déplacer dessus, elles sont de fonds noirs.
		- Les cases sur fond rouge représentent les monstres, ce qu'il y a écrit à l'intérieur diffèrent selon les différents monstres ("Ti" pour un Tigre, "Wo" pour un Loup etc.)
		- Les cases sur fond blanc où est inscrit "Lt" en bleu symbolisent les Loot, c'est à dire des objets ou ressources que vous pouvez ramasser en vous déplacant dessus.

- En bas se trouve les informations concernant vos points d'action, votre nom ainsi que votre vie. C'est également ici que s'afficheront les données de combat comme le nombre de dégat infligés, ou encore les objets ramassés.

- En bas à droite se trouve vos caractéristiques, elles peuvent varier en fonction de votre équipement.

- Enfin en haut à droite se trouve le menu du jeu séparé en plusieurs actions (pour se déplacer dans le menu, utiliser les touches fléchées ainsi que les touches entrée et echap):
	- Tout d'abord le nom du mode dans lequel vous êtes.
	- Le mode de déplacement : utiliser les touches fléchés après activation de ce mode pour vous déplacer (les numéros affichés autour de vous correspondent au nombre de points d'actions nécéssaires afin d'aller à cet emplacement.)
	- Le mode d'attaque, affichant un ennemi s'il est a porté d'attaque et permettant de l'attaquer.
	- Le mode "Utiliser", permettant d'utiliser un objet consommable (comme les potions par exemple).
	- Le mode "Inventaire", lorsque vous utilisez ce mode, un nouvelle fenêtre s'affiche, affichant votre inventaire à gauche et votre équipement à droite. Un message en bas de l'écran vous rappelle les commandes à utiliser, vous pouvez équiper ou déséquiper des objets. Vous pouvez également accéder au menu de craft via la commande "C", affichant une nouvelle fenêtre vous permettant de fabriquer des objets en utilisant ce que vous posséder dans votre inventaire.
	- Le mode "Terminer mon tour", terminera votre tour et laissera les autres personnages sur la map se déplacer ou attaquer en fonction de leurs points d'actions.
	- Le mode "Sauvegarder" sauvegardera votre progression, vous pourrez ainsi reprendre votre partie plus tard.
	- Le mode "Retournez au menu principal" retournera au menu principal du jeu tout en vous demandant confirmation.

Charger partie :

Cette option n'est pas encore implémentée car nous nous sommes heurtés à certain problème de lecture des sauvegardes.

Options :

Le mode option vous permet de changer la langue du jeu grâce aux touches fléchées.

Quitter :

Ferme la fenêtre et quitte le jeu.


Easter Egg :

Dans les langages se trouve un langage dénommé "Troll", accroissant drastiquement la difficulté du jeu : Résérvé aux initiés.

Bestiaire :

"Wo" = Loup : monstre de rang 1.
"Ti" = Tigre : monstre de rang 2.
"Br" = Ours : monstre de rang 3.
"!!" = T-Rex : Boss du jeu.