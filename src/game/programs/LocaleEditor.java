package game.programs;

import game.lang.MessageRepository;
import game.util.ConsoleKeyboard;
import game.util.Logger;

import java.util.Map.Entry;

public final class LocaleEditor {
	
	private static Boolean changedSinceLastSave = false;
	
	public static void
	confirmForSave()
	{
		if (changedSinceLastSave)
		{
			Logger.log(" - Des changements ont ete effectues, voulez-vous les sauvegarder ? [o/n]:");
			String confirmation = ConsoleKeyboard.readWord();
			if (confirmation.equals("o"))
			{
				MessageRepository.saveToJson();
			} else if (!confirmation.equals("n"))
			{
				confirmForSave();
			}
		}
		changedSinceLastSave = false;
	}
	
	public static void
	main(String[] args) {
		while (!menu()) {}
		ConsoleKeyboard.close();
	}
	
	public static Boolean
	menu()
	{
		Logger.clear();
		Logger.flush();
		Logger.logLine(" ----------------------------------------------- ");
		Logger.logLine("|                                               |");
		Logger.logLine("|                 LOCALE EDITOR                 |");
		Logger.logLine("|                  version 0.3                  |");
		Logger.logLine("|                                               |");
		Logger.logLine(" ----------------------------------------------- ");
		Logger.line();
		Logger.logLine(" * Liste des operations:");
		Logger.line();
		Logger.logLine("\t 1. Changer le locale courant (actuellement '"+MessageRepository.gameLocale()+"')");
		Logger.logLine("\t 2. Voir la liste des messages");
		Logger.logLine("\t 3. Voir un message via son id");
		Logger.logLine("\t 4. Creer un nouveau message");
		Logger.logLine("\t 5. Supprimer un message");
		Logger.logLine("\t 6. Sauvegarder la liste des messages");
		Logger.logLine("\t 7. Quitter");
		Logger.line	  ();
		Logger.log    (" - Choisissez l'operation a effectuer [1-7]: ");
		Integer choice = ConsoleKeyboard.readInt();
		
		switch (choice)
		{
			/* Changer le locale courant */
			case 1:
			{
				confirmForSave();
				Logger.log(" - Tapez le code ISO 639-1 du locale: ");
				String code = ConsoleKeyboard.readWord();
				MessageRepository.forceLocale(code);
				break;
			}

			/* Lister les messages */
			case 2:
			{
				if (MessageRepository.cachedMessages().size() == 0)
				{
					Logger.logLine("Il n'y a aucun message a afficher.");
					ConsoleKeyboard.waitEnterKey();
					break;
				}
				
				for (Entry<String, String> entry : MessageRepository.cachedMessages().entrySet())
				{
					Logger.logLine("\t" + entry.getKey() + ": \"" + entry.getValue() + "\"");
				}
				ConsoleKeyboard.waitEnterKey();
				break;
			}

			/* Voir un message via son id */
			case 3:
			{
				Logger.log(" - Entrez l'identifiant du message: ");
				String id = ConsoleKeyboard.readWord();
				
				if (MessageRepository.cachedMessages().containsKey(id))
				{
					Logger.logLine("\t"+ id + ": \"" + MessageRepository.cachedMessages().get(id) + "\"");
				} else
				{
					Logger.logLine(" - Il n'y a pas de message pour la cle \"" + id + "\".");
				}
				
				ConsoleKeyboard.waitEnterKey();
				break;
			}
			
			/* Creer un nouveau message */
			case 4:
			{
				while (!newMessageAssistant()) {}
				break;
			}
			
			/* Supprime un message via son id */
			case 5:
			{
				Logger.log(" - Entrez l'identifiant du message: ");
				String id = ConsoleKeyboard.readWord();
				if (MessageRepository.cachedMessages().containsKey(id))
				{
					Logger.log(" - Voulez-vous supprimer le message \""
							+ MessageRepository.cachedMessages().get(id) + "\" ? [o/n]: ");
					
					String confirmation = ConsoleKeyboard.readWord();
					if (confirmation.equals("o"))
					{
						MessageRepository.cachedMessages().remove(id);
						Logger.logLine(" - Le message a ete supprimee.");
					} else
					{
						Logger.logLine(" - Suppression du message annulee.");
					}
				} else
				{
					Logger.logLine(" - Il n'y a pas de message pour la clee \"" + id + "\". Impossible de le supprimer.");
				}
				ConsoleKeyboard.waitEnterKey();
				break;
			}

			/* Sauver la liste des messages */
			case 6:
			{
				MessageRepository.saveToJson();
				break;
			}

			/* Quitter */
			case 7:
			{
				confirmForSave();
				return true; // On quitte l'editeur
			}
				
			default:
			{
				break;
			}
		}
		
		return false; // On ne quitte pas l'editeur
	}
	
	private static Boolean
	newMessageAssistant()
	{
		Logger.log(" - Entrer l'identifiant du message: ");
		String id = ConsoleKeyboard.readWord();
		
		if (MessageRepository.cachedMessages().containsKey(id))
		{
			Logger.log(" - Cet identifiant existe deja, voulez-vous le remplacer par un nouveau message? [o/n]: ");
			String confirmation = ConsoleKeyboard.readWord();
			if (!confirmation.equals("o"))
			{
				return false;
			}
		}
		Logger.log(" - Entrer le contenu du message #" + id + ": ");
		String message = ConsoleKeyboard.readLine();
		
		MessageRepository.addMessage(id, message);
		
		return true;
	}
}
