package game.programs;

import game.environment.GrassTile;
import game.environment.Map;
import game.environment.SandTile;
import game.environment.WallTile;
import game.util.DelayedPathfinder;
import game.util.IPathfinderDelegate;
import game.util.IntPair;
import game.util.Logger;

import java.util.ArrayList;

public final class PathfindingTest implements Runnable, IPathfinderDelegate {

	public static void main(String[] args)
	{
		new PathfindingTest().run();
	}
	
	public PathfindingTest() {
		
	}

	@Override
	public void pathFound(DelayedPathfinder finder, ArrayList<IntPair> path) {
		Logger.logLine("Path found!");
		Logger.logLine(path);
	}

	@Override
	public void run() {
		Map m = new Map("map test", new IntPair(5,5), 1);
		m.fillLayer(0, GrassTile.class);
		m.tileLayer(0).set(new IntPair(0,1), new SandTile());
		//m.tileLayer(0).set(new IntPair(1,0), new WallTile());
		m.tileLayer(0).set(new IntPair(1,1), new WallTile());
		m.tileLayer(0).set(new IntPair(1,2), new WallTile());
		m.tileLayer(0).set(new IntPair(2,2), new WallTile());
		m.tileLayer(0).set(new IntPair(3,2), new WallTile());
		m.tileLayer(0).set(new IntPair(3,1), new WallTile());
		//m.tileLayer(0).set(new IntPair(3,0), new WallTile()); // si aucun chemin n'est possible, null est renvoyé
		new DelayedPathfinder(m, new IntPair(0,0), new IntPair(2,1), this, false);
	}
	
}
