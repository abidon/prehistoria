package game.programs;

import game.server.ServerThread;
import game.util.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public final class ServerLauncher {
	
	private static ArrayList<ServerThread>
	threads = new ArrayList<ServerThread>();
	
	@SuppressWarnings("static-access")
	public static void
	main(String[] args)
	{
		ServerSocket listenSocket = null;
		try {
			listenSocket = new ServerSocket(65533);
		} catch (IOException e) {
			Logger.logDateAndTime().logLine(": Unable to start the server listener.");
			e.printStackTrace();
			return;
		}
		Logger.logDateAndTime().logLine(": Starting the server listener on port " + listenSocket.getLocalPort() + ".");
		
		while (true)
		{
			if (listenSocket == null) break;
			
			try {
				Socket clientSocket = listenSocket.accept();
				ServerThread st = new ServerThread(clientSocket);
				st.setName("ClientThread(address=" + clientSocket.getInetAddress());
				threads.add(st);
				st.run();
			} catch (IOException e) {
				Logger.logDateAndTime().logLine("Unable to accept client connection.");
				e.printStackTrace();
				continue;
			}
		}
		
		try {
			listenSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
