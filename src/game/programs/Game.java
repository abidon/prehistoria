package game.programs;

import game.characters.PlayableCharacter;
import game.lang.MessageRepository;
import game.util.FrameratedScreen;
import game.util.Logger;
import game.views.TrademarksView;
import game.views.View;

import java.lang.reflect.Constructor;

import com.googlecode.lanterna.TerminalFacade;
import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.swing.SwingTerminal;

public final class Game implements Runnable {
	
	public final static Game instance = new Game();
	
	public static void
	main(String[] args) {
		instance.run();
	}
	private PlayableCharacter player = null;
	private FrameratedScreen screen;
	
	private boolean shouldQuit = false;
	
	private SwingTerminal terminal;
	private View view = null;
	
	public
	Game()
	{
		super();
		this.terminal = TerminalFacade.createSwingTerminal(120, 34);
		this.screen = new FrameratedScreen(terminal);
		this.screen.setFramerateLimit(60);
		this.screen.showFramerate(true);
	}
	
	public View
	currentView()
	{
		return this.view;
	}
	
	public void instanciateMainPlayer(String playerClass, String name)
	{
		try {
			Constructor<?> constructor = Class.forName(playerClass).getConstructor(String.class);
			this.player = (PlayableCharacter)constructor.newInstance(name);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public PlayableCharacter mainPlayer() {
		return player;
	}

	private void
	processKeyboardEvents(Key k)
	{
		switch (k.getKind())
		{
			case NormalKey:
			{
				switch (k.getCharacter())
				{
					default:
					{
						break;
					}
				}
			}
			
			case Escape:
			{
				
			}
			
			case ArrowLeft:
			{
				
			}
			
			case ArrowRight:
			{
				
			}
			
			case ArrowDown:
			{
				
			}
			
			case ArrowUp:
			{
				
			}
			
			case Tab:
			{
				
			}
			
			case Enter:
			{
				
			}
			
			default:
			{
				
			}
		}
	}
	
	@SuppressWarnings("static-access")
	@Override
	public void
	run()
	{
		Logger.logDateAndTime().logLine(": Launching " + MessageRepository.messageForId("gameName"));
		
		this.screen.startScreen();
		this.setView(new TrademarksView());
		this.screen.setCursorPosition(this.screen.getTerminalSize().getColumns()-1, this.screen.getTerminalSize().getRows()-1);
		
		while(this.shouldQuit == false)
		{
			Key k = this.screen.readInput();
			while (k != null)
			{
				processKeyboardEvents(k);
				
				if (this.view != null)
					this.view.processKeyboardEvents(k);
				
				k = this.screen.readInput();
			}
			
			if (this.view != null)
				this.view.update();
		}
		
		game.audio.AudioPlayer.stopAll();
		
		this.screen.stopScreen();
		Logger.logDateAndTime().logLine(": Stopping " + MessageRepository.messageForId("gameName"));
	}

	public void setShouldQuit() {
		this.shouldQuit = true;
	}
	
	public void
	setView(View view)
	{
		if (this.view != null) this.view.removeFromScreen();
		
		this.view = view;
		this.view.showOnScreen(this.screen);
	}
}
