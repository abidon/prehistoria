package game.items;

import game.lang.MessageRepository;

public abstract class Item {

	private String name;
	
	public Item(String name) {
		super();
		
		this.name = name;
	}
	
	public String
	name()
	{
		return MessageRepository.messageForId(this.name);
	}
	
	public abstract int
	sellPrice();
	
	public abstract int
	weight();

}
