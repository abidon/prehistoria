package game.items;

public class RecipeStoneWeapon extends Recipe {

	public RecipeStoneWeapon() {
		super("RecipeStoneWeapon", new RecipeRequirement(Stone.class, 4));
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Class craftedItemKind() {
		return StoneWeapon.class;
	}

}
