package game.items;

import game.util.Pair;

@SuppressWarnings("rawtypes")
public final class RecipeRequirement extends Pair<Class, Integer> {

	public RecipeRequirement(Class first, Integer second) {
		super(first, second);
	}

	public RecipeRequirement(Pair<Class, Integer> copy) {
		super(copy);
	}

}
