package game.items;

public class WoodStaff extends Weapon {
	
	public WoodStaff()
	{
		super("WoodStaff");
	}

	@Override
	public int 
	dexterityBonus() {
		return 0;
	}

	@Override
	public int 
	hpBonus() {
		return 0;
	}

	@Override
	public int 
	range() {
		return 1;
	}

	@Override
	public int 
	resistanceBonus() {
		return 0;
	}

	@Override
	public int 
	sellPrice() {
		return 5;
	}

	@Override
	public int 
	strengthBonus() {
		return 1;
	}
	
	@Override
	public int 
	weight() {
		return 3;
	}
}
