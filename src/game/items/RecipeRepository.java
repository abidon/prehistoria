package game.items;

import java.util.ArrayList;

public class RecipeRepository {

	private static ArrayList<Class<?>> recipes;
	
	static {
		recipes = new ArrayList<Class<?>>();
		addRecipe(RecipeGranitBoots.class);
		addRecipe(RecipeGranitRing.class);
		addRecipe(RecipeGranitWeapon.class);
		addRecipe(RecipeHealthPotion.class);
		addRecipe(RecipeStoneBoots.class);
		addRecipe(RecipeStoneRing.class);
		addRecipe(RecipeStoneWeapon.class);
		addRecipe(RecipeWoodBoots.class);
		addRecipe(RecipeWoodStaff.class);
		addRecipe(RecipeWoodRing.class);
	}
	
	public static void
	addRecipe(Class<?> c)
	{
		try {
			if(c.newInstance() instanceof Recipe)
				recipes.add(c);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
	public static ArrayList<Class<?>>
	recipeList()
	{
		return recipes;
	}

}
