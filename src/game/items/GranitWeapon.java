package game.items;

public class GranitWeapon extends Weapon {

	public GranitWeapon() {
		super("GranitWeapon");
	}

	@Override
	public int range() {
		return 1;
	}

	@Override
	public int dexterityBonus() {
		return 0;
	}

	@Override
	public int hpBonus() {
		return 0;
	}

	@Override
	public int resistanceBonus() {
		return 4;
	}

	@Override
	public int strengthBonus() {
		return 8;
	}

	@Override
	public int sellPrice() {
		return 15;
	}

	@Override
	public int weight() {
		return 7;
	}

}
