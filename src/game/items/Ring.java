/**
 * @author Emeric de Peretti
 */

package game.items;

public abstract class Ring extends Equipment {

	public Ring(String name) {
		super(name);
	}

}
