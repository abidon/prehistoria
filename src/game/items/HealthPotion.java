package game.items;

import game.characters.Character;
import game.characters.PlayableCharacter;

public class HealthPotion extends Usable {
	
	private final int hpRecovery;
	
	public HealthPotion(int hpRecovery) {
		super("HealthPotion");
		this.hpRecovery = hpRecovery;
	}
	
	@Override
	public String name()
	{
		return (super.name() + " +" + this.hpRecovery);
	}

	@Override
	public int neededAP() {
		return 2;
	}

	@Override
	public int possibleUses() {
		return 1;
	}

	@Override
	public int use(Character user, Character target) {
		if (user instanceof PlayableCharacter)
		{
			((PlayableCharacter)user).inventorySet().deleteItem(this);
		}
		
		target.setCurrentHP(target.currentHP() + this.hpRecovery);
		
		return 0;
	}

	@Override
	public int sellPrice() {
		return 10;
	}

	@Override
	public int weight() {
		return 1;
	}
	
	
	
}
