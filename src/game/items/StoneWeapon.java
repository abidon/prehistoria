package game.items;

public class StoneWeapon extends Weapon {

	public StoneWeapon() {
		super("StoneWeapon");
	}

	@Override
	public int range() {
		return 1;
	}

	@Override
	public int dexterityBonus() {
		return 0;
	}

	@Override
	public int hpBonus() {
		return 0;
	}

	@Override
	public int resistanceBonus() {
		return 2;
	}

	@Override
	public int strengthBonus() {
		return 5;
	}

	@Override
	public int sellPrice() {
		return 10;
	}

	@Override
	public int weight() {
		return 5;
	}

}
