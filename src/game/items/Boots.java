/**
 * @author Emeric de Peretti
 */

package game.items;

public abstract class Boots extends Equipment {
	
	public Boots(String name){
		super(name);
	}

}
