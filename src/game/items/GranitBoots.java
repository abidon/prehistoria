package game.items;

public class GranitBoots extends Boots {

	public GranitBoots() {
		super("GranitBoots");
	}

	@Override
	public int dexterityBonus() {
		return 6;
	}

	@Override
	public int hpBonus() {
		return 4;
	}

	@Override
	public int resistanceBonus() {
		return 5;
	}

	@Override
	public int strengthBonus() {
		return 0;
	}

	@Override
	public int sellPrice() {
		return 15;
	}

	@Override
	public int weight() {
		return 7;
	}

}
