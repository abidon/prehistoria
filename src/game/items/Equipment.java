package game.items;

public abstract class Equipment extends Item {

	public Equipment(String name) {
		super(name);
	}
	
	public abstract int
	dexterityBonus();
	
	public int
	entrepriseBonus()
	{
		int rolls = this.dexterityBonus() / 3;
		int add = this.dexterityBonus() % 3;
		int entreprise = 0;
		for (int i = 0; i < rolls; i++)
		{
			entreprise += (int)Math.random()%6;
		}
		entreprise += add;
		
		return entreprise;
	}
	
	public int
	escapeChanceBonus()
	{
		int rolls = this.dexterityBonus() / 6;
		int add = this.dexterityBonus() % 3;
		int escape = 0;
		for (int i = 0; i < rolls; i++)
		{
			escape += (int)Math.random()%4;
		}
		escape += add;
		
		return escape;
	}
	
	public int
	guardEfficiencyBonus()
	{
		int rolls = this.resistanceBonus() / 3;
		int add = this.resistanceBonus() % 3;
		int guard = 0;
		for (int i = 0; i < rolls; i++)
		{
			guard += (int)Math.random()%6;
		}
		guard += add;
		
		return guard;
	}
	
	public int
	hitChanceBonus()
	{
		int rolls = this.dexterityBonus() / 4;
		int add = this.dexterityBonus() % 4;
		int hitChance = 0;
		for (int i = 0; i < rolls; i++)
		{
			hitChance += (int)Math.random()%3;
		}
		hitChance += add;
		
		return hitChance;
	}
	
	public abstract int
	hpBonus();
	
	public int
	rawDamageBonus()
	{
		int rolls = this.strengthBonus() / 3;
		int add = this.strengthBonus() % 3;
		int rawDamage = 0;
		for (int i = 0; i < rolls; i++)
		{
			rawDamage += (int)Math.random()%6;
		}
		rawDamage += add;
		
		return rawDamage;
	}
	
	public abstract int
	resistanceBonus();
	
	public abstract int
	strengthBonus();

}
