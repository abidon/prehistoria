package game.items;

import game.characters.Character;

public abstract class Usable extends Item {
	
	public Usable(String name) {
		super(name);
	}
	
	public abstract int
	neededAP();
	
	public abstract int
	possibleUses();
	
	public abstract int
	use(Character user, Character target);

}
