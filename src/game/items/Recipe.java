package game.items;

import game.characters.PlayableCharacter;
import game.lang.MessageRepository;

import java.util.HashMap;
import java.util.Map.Entry;

public abstract class Recipe {
	
	private final String name;
	private HashMap<Class<?>, Integer> requirements;
	
	public
	Recipe(String name, RecipeRequirement... requirements)
	{
		super();
		
		this.name = name;
		this.requirements = new HashMap<Class<?>, Integer>(requirements.length);
		
		for (RecipeRequirement requirement : requirements)
		{
			if (this.requirements.containsKey(requirement.first()))
			{
				this.requirements.put(requirement.first(), this.requirements.get(requirement.first()) + requirement.second());
			} else {
				this.requirements.put(requirement.first(), requirement.second());
			}
		}
	}
	
	public boolean
	craft(PlayableCharacter c)
	{
		if (c == null) return false;
		if (this.hasRequiredResources(c) == false) return false;
		
		InventorySet cInventory = c.inventorySet();
		
		/**
		 *  On ajoute d'abord l'objet a l'inventaire.
		 *  Si une exception est levée, les "ingrédients" n'auront pas été
		 *  enlevés de l'inventaire.
		*/
		try {
			Object object = craftedItemKind().newInstance();
			if (object instanceof Item)
				cInventory.addItem((Item)object);
			else
				return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		for (Entry<Class<?>, Integer> e : this.requirements.entrySet())
		{
			for (int i = 0; i < e.getValue(); i++)
			{
				cInventory.deleteItemOfKind(e.getKey());
			}
		}
		
		return true;
	}
	
	@SuppressWarnings("rawtypes")
	public abstract
	Class
	craftedItemKind();

	public boolean
	hasRequiredResources(PlayableCharacter c)
	{
		if (c == null) return false;
		InventorySet cInventory = c.inventorySet();
		
		for (Entry<Class<?>, Integer> e : this.requirements.entrySet())
		{
			if (cInventory.itemsCountOfKind(e.getKey()) < e.getValue())
				return false;
		}
		
		return true;
	}
	
	public String
	name()
	{
		return MessageRepository.messageForId(this.name);
	}
	
	public HashMap<Class<?>, Integer> 
	requirements() 
	{
		return this.requirements;
	}
	
}
