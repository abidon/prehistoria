package game.items;

public class WoodBoots extends Boots {

	public WoodBoots() {
		super("WoodBoots");
	}

	@Override
	public int dexterityBonus() {
		return 2;
	}

	@Override
	public int hpBonus() {
		return 1;
	}

	@Override
	public int resistanceBonus() {
		return 1;
	}

	@Override
	public int sellPrice() {
		return 5;
	}

	@Override
	public int strengthBonus() {
		return 0;
	}

	@Override
	public int weight() {
		return 2;
	}

}
