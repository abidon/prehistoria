package game.items;

public class Meat extends Resource {

	public Meat() {
		super("Meat");
	}

	@Override
	public int sellPrice() {
		return 3;
	}

	@Override
	public int weight() {
		return 1;
	}

}
