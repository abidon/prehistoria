package game.items;

public class RecipeGranitBoots extends Recipe {

	public RecipeGranitBoots() {
		super("RecipeGranitBoots", new RecipeRequirement(Granit.class, 5));
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Class craftedItemKind() {
		return GranitBoots.class;
	}

}
