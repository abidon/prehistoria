package game.items;

import java.util.ArrayList;
import java.util.Iterator;

public class InventorySet implements Iterable<Item> {
	
	private static final int MAX_SLOTS = 200;
	private static final int MAX_WEIGHT = 200;
	private final ArrayList<Item> items;
	
	public InventorySet() {
		super();
		
		this.items = new ArrayList<Item>(MAX_SLOTS);
	}
	
	public boolean
	addItem(Item i)
	{
		if ((itemsCount() == MAX_SLOTS) || (totalWeight() + i.weight() > MAX_WEIGHT)) return false;
		
		items.add(i);
		return true;
	}
	
	public void
	deleteItem(int index)
	{
		this.items.remove(index);
	}
	
	public void
	deleteItem(Item i)
	{
		this.items.remove(i);
	}
	
	@SuppressWarnings("rawtypes")
	public void
	deleteItemOfKind(Class c)
	{
		for (Item i : items)
		{
			if (i.getClass().equals(c))
			{
				items.remove(i);
				return;
			}
		}
	}
	
	public ArrayList<Item> 
	inventoryList()
	{
		return this.items;
	}
	
	public int
	itemIndex(Item i)
	{
		return this.items.indexOf(i);
	}
	
	public int
	itemsCount()
	{
		return this.items.size();
	}

	@SuppressWarnings("rawtypes")
	public int
	itemsCountOfKind(Class c)
	{
		int count = 0;
		for (Item i : items)
		{
			if (i.getClass().equals(c)) count += 1;
		}
		return count;
	}
	
	@SuppressWarnings("rawtypes")
	public ArrayList<Item>
	itemsOfKind(Class c)
	{
		ArrayList<Item> returnItems = new ArrayList<Item>();
		for (Item i : items)
		{
			if (c.isInstance(i)) returnItems.add(i);
		}
		return returnItems;
	}
	
	@Override
	public Iterator<Item> iterator() {
		return new Iterator<Item>() {
			private int i = 0;
			
			@Override
			public boolean hasNext() {
				while (i < items.size())
					return true;
				return false;
			}

			@Override
			public Item next() {
				return items.get(i++);
			}

			@Override
			public void remove() {
				items.remove(i);
			}
		};
	}
	
	public Item
	selectItem(int index)
	{
		return this.items.get(index);
	}

	public Item
	selectItem(Item i)
	{
		return this.items.get(this.items.indexOf(i));
	}
	
	public int
	totalWeight()
	{
		int totalWeight = 0;
		for (Item i : items)
		{
			totalWeight += i.weight();
		}
		return totalWeight;
	}
	
}
