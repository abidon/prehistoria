package game.items;

public class StoneRing extends Ring {

	public StoneRing(String name) {
		super("StoneRing");
	}

	@Override
	public int dexterityBonus() {
		return 4;
	}

	@Override
	public int hpBonus() {
		return 3;
	}

	@Override
	public int resistanceBonus() {
		return 0;
	}

	@Override
	public int strengthBonus() {
		return 2;
	}

	@Override
	public int sellPrice() {
		return 7;
	}

	@Override
	public int weight() {
		return 3;
	}

}
