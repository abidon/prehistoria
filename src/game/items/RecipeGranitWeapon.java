package game.items;

public class RecipeGranitWeapon extends Recipe {

	public RecipeGranitWeapon() {
		super("RecipeGranitWeapon", new RecipeRequirement(Granit.class, 4));
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Class craftedItemKind() {
		return GranitWeapon.class;
	}

}
