package game.items;

public class EndGameResource extends Resource {

	public EndGameResource() {
		super("EndGameResource");
	}

	@Override
	public int sellPrice() {
		return 0;
	}

	@Override
	public int weight() {
		return 0;
	}

}
