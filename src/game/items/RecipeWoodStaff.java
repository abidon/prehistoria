package game.items;

public class RecipeWoodStaff extends Recipe {

	public RecipeWoodStaff() {
		super("RecipeWoodStaff", new RecipeRequirement(Stick.class, 4));
		
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Class craftedItemKind() {
		return WoodStaff.class;
	}

}
