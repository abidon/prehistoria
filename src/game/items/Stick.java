package game.items;

public class Stick extends Resource {

	public Stick() {
		super("Stick");
	}

	@Override
	public int sellPrice() {
		return 1;
	}

	@Override
	public int weight() {
		return 1;
	}

}
