package game.items;

public class RecipeHealthPotion extends Recipe {

	public RecipeHealthPotion() {
		super("RecipeHealthPotion", new RecipeRequirement(Meat.class, 2));
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Class craftedItemKind() {
		return HealthPotion.class;
	}

}
