/**
 * @author Emeric de Peretti
 */

package game.items;

import game.programs.Game;

import java.util.ArrayList;

public class EquipmentSet {
	
	/**
	 * La paire de botte équipée par le Character.
	 */
	private Boots boots;
	
	/**
	 * Les anneaux équipés par le Character.
	 */
	private Ring[] rings;
	
	/**
	 * L'arme équipé par le Character.
	 */
	private Weapon weapon;
	 
	/**
	 * Constructeur par défaut
	 */
	public
	EquipmentSet()
	{
		super();
		
		this.boots = null;
		this.rings = new Ring[]{null,null};
		this.weapon = null;
	}
	
	/**
	 * Methode pour récupérer les bottes équipées d'un Character.
	 * @return Les bottes équipées par le Character.
	 */
	public Boots
	boots()
	{
		return this.boots;
	}
	
	/**
	 * Calcule la somme de la Déxtérité de tous les équipements équipés et la retourne.
	 * @return La déxtérité bonus apportée par tout l'équipement équipé.
	 */
	public int
	dexterityBonus()
	{
		int dexterity=0;
		for(Equipment equipment : equipmentList())
		{
			dexterity += equipment.dexterityBonus();
		}
		return dexterity;
	}
	
	/**
	 * Calcule la somme de l'entreprise de tous les équipements équipés et la retourne.
	 * @return L'entreprise bonus apportée par tout l'équipement équipé.
	 */
	public int
	entrepriseBonus()
	{
		int entreprise=0;
		for(Equipment equipment : equipmentList())
		{
			entreprise += equipment.entrepriseBonus();
		}
		return entreprise;
	}
	
	/**
	 * Rassemble tous les équipements équipés dans une ArrayList.
	 * @return Une ArrayList contenant les équipements équipés.
	 */
	public ArrayList<Equipment>
	equipmentList()
	{
		ArrayList<Equipment> list = new ArrayList<Equipment>();
		if(weapon()!=null)
			list.add(weapon());
		
		if(boots()!=null)
			list.add(boots());
		
		if(leftRing()!=null)
			list.add(leftRing());
		
		if(rightRing()!=null)
			list.add(rightRing());
		
		return list;
	}
	
	/**
	 * Calcule la somme de l'escapeChance de tous les équipements équipés et la retourne.
	 * @return L'entreprise bonus apportée par tout l'équipement équipé.
	 */
	public int
	escapeChanceBonus()
	{
		int escapeChance=0;
		for(Equipment equipment : equipmentList())
		{
			escapeChance += equipment.escapeChanceBonus();
		}
		return escapeChance;
	}
	
	/**
	 * Calcule la somme de la guardEfficiency de tous les équipements équipés et la retourne.
	 * @return La guardEfficiency bonus apportée par tout l'équipement équipé.
	 */
	public int
	guardEfficiencyBonus()
	{
		int guardEfficiency=0;
		for(Equipment equipment : equipmentList())
		{
			guardEfficiency += equipment.guardEfficiencyBonus();
		}
		return guardEfficiency;
	}
	
	/**
	 * Calcule la somme de la hitChance de tous les équipements équipés et la retourne.
	 * @return La hitChance bonus apportée par tout l'équipement équipé.
	 */
	public int
	hitChanceBonus()
	{
		int hitChance=0;
		for(Equipment equipment : equipmentList())
		{
			hitChance += equipment.hitChanceBonus();
		}
		return hitChance;
	}
	
	/**
	 * Calcule la somme des hp de tous les équipements équipé et la retourne.
	 * @return Les hp bonus apportée par tout l'équipement équipé.
	 */
	public int
	hpBonus()
	{
		int hp=0;
		for(Equipment equipment : equipmentList())
		{
			hp += equipment.hpBonus();
		}
		return hp;
	}
	
	/**
	 * Methode determinant si l'inventaire équipé est plein.
	 * @return Un booléen indiquant si l'inventaire équipé est plein.
	 */
	public boolean
	isFull()
	{
		if (this.boots!=null && this.rings[0]!=null && this.rings[1]!=null && this.weapon!=null)
		{
			return true;
		}
		
		else
		{
			return false;
		}
	}
	
	/**
	 * Une méthode retournant le ring équipé gauche.
	 * @return Un ring.
	 */
	public Ring
	leftRing()
	{
		return this.rings[0];
	}
	
	/**
	 * Calcule la somme des rawDamage de tous les équipements équipés et la retourne.
	 * @return Les rawDamage bonus apportée par tout l'équipement équipés.
	 */
	public int
	rawDamageBonus()
	{
		int rawDamage=0;
		for(Equipment equipment : equipmentList())
		{
			rawDamage += equipment.rawDamageBonus();
		}
		return rawDamage;
	}
	
	/**
	 * Calcule la somme des resistance de tous les équipements équipés et la retourne.
	 * @return Les resistance bonus apportée par tout l'équipement équipés.
	 */
	public int
	resistanceBonus()
	{
		int resistance=0;
		for(Equipment equipment : equipmentList())
		{
			resistance += equipment.resistanceBonus();
		}
		return resistance;
	}
	
	/**
	 * Une méthode retournant le ring équipé droit.
	 * @return Un ring.
	 */
	public Ring
	rightRing()
	{
		return this.rings[1];
	}
	
	/**
	 * Une méthode retournant les ring équipés.
	 * @return Un tableau de ring.
	 */
	public Ring[]
	rings()
	{
		return this.rings;
	}
	
	/**
	 * Une méthode permettant de changer les bottes équipés ou d'en équiper des nouvelles.
	 * @param boots Des bottes.
	 */
	public void
	setBoots(Boots boots)
	{
		this.boots = boots;
	}
	
	public void
	setItem(Item item)
	{
		boolean changeCurrentHp = false;
		if(Game.instance.mainPlayer().currentHP() == Game.instance.mainPlayer().maxHP())
			changeCurrentHp = true;
		if(item instanceof Weapon)
			this.setWeapon((Weapon)item);
		if(item instanceof Ring)
			this.setRing((Ring)item);
		if(item instanceof Boots)
			this.setBoots((Boots)item);
		if(changeCurrentHp)
			Game.instance.mainPlayer().setCurrentHP(Game.instance.mainPlayer().maxHP());
		
		Game.instance.mainPlayer().inventorySet().deleteItem(item);
		
	}
	
	/**
	 * Une méthode permettant de changer le ring gauche équipé ou d'en équiper un nouveau.
	 * @param ring Un ring.
	 */
	public void
	setLeftRing(Ring ring)
	{
		this.rings[0] = ring;
	}
	
	/**
	 * Une méthode permettant de changer le ring droit équipé ou d'en équiper un nouveau.
	 * @param ring Un ring.
	 */
	public void
	setRightRing(Ring ring)
	{
		this.rings[1] = ring;
	}
	
	public void
	setRing(Ring ring)
	{
		if(this.rings[0]==null)
		{
			this.rings[0] = ring;
			return;
		}
		if(this.rings[1]==null)
		{
			this.rings[1] = ring;
			return;
		}
		this.rings[0] = ring;
	}
	
	/**
	 * Une méthode permettant de changer la weapon équipé ou d'en équiper une nouvelle.
	 * @param weapon Un weapon.
	 */
	public void
	setWeapon(Weapon weapon)
	{
		this.weapon = weapon;
	}
	
	/**
	 * Calcule la somme des strength de tous les équipements équipés et la retourne.
	 * @return Les strength bonus apportée par tout l'équipement équipés.
	 */
	public int
	strengthBonus()
	{
		int strength=0;
		for(Equipment equipment : equipmentList())
		{
			strength += equipment.strengthBonus();
		}
		return strength;
	}
	
	public void
	unsetItem(Item item)
	{
		if(item instanceof Weapon)
			this.weapon = null;
		if(item instanceof Ring)
		{
			if(item.equals(rings[0]))
				this.rings[0] = null;
			else
				this.rings[1] = null;
		}
		if(item instanceof Boots)
			this.boots = null;
		Game.instance.mainPlayer().inventorySet().addItem(item);
	}
	
	/**
	 * Une méthode retournant la weapon équipée.
	 * @return Une weapon
	 */
	public Weapon
	weapon()
	{
		return this.weapon;
	}
}
