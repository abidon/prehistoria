package game.items;

public class RecipeWoodBoots extends Recipe {

	public RecipeWoodBoots() {
		super("RecipeWoodBoots", new RecipeRequirement(Stick.class , 5));
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Class craftedItemKind() {
		return WoodBoots.class;
	}

}
