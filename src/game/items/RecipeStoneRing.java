package game.items;

public class RecipeStoneRing extends Recipe {

	public RecipeStoneRing() {
		super("RecipeStoneRing", new RecipeRequirement(Stone.class, 2));
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Class craftedItemKind() {
		return StoneRing.class;
	}

}
