package game.items;


public class Stone extends Resource {

	public Stone() {
		super("Stone");
	}

	@Override
	public int sellPrice() {
		return 2;
	}

	@Override
	public int weight() {
		return 2;
	}

}
