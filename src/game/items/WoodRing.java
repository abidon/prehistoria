package game.items;

public class WoodRing extends Ring {

	public WoodRing() {
		super("WoodRing");
	}

	@Override
	public int dexterityBonus() {
		return 2;
	}

	@Override
	public int hpBonus() {
		return 1;
	}

	@Override
	public int resistanceBonus() {
		return 0;
	}

	@Override
	public int sellPrice() {
		return 5;
	}

	@Override
	public int strengthBonus() {
		return 1;
	}

	@Override
	public int weight() {
		return 1;
	}

}
