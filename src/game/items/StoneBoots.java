package game.items;

public class StoneBoots extends Boots {

	public StoneBoots() {
		super("StoneBoots");
	}

	@Override
	public int dexterityBonus() {
		return 4;
	}

	@Override
	public int hpBonus() {
		return 2;
	}

	@Override
	public int resistanceBonus() {
		return 3;
	}

	@Override
	public int strengthBonus() {
		return 0;
	}

	@Override
	public int sellPrice() {
		return 10;
	}

	@Override
	public int weight() {
		return 5;
	}

}
