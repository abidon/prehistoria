package game.items;

public class RecipeStoneBoots extends Recipe {

	public RecipeStoneBoots() {
		super("RecipeStoneBoots", new RecipeRequirement(Stone.class , 5));
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Class craftedItemKind() {
		return StoneBoots.class;
	}

}
