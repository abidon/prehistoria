package game.items;

public class RecipeGranitRing extends Recipe {

	public RecipeGranitRing() {
		super("RecipeGranitRing", new RecipeRequirement(Granit.class, 2));
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Class craftedItemKind() {
		return GranitRing.class;
	}

}
