package game.items;

public class Granit extends Resource {

	public Granit() {
		super("Granit");
	}

	@Override
	public int sellPrice() {
		return 5;
	}

	@Override
	public int weight() {
		return 4;
	}

}
