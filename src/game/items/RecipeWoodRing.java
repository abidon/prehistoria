package game.items;

public class RecipeWoodRing extends Recipe {

	public RecipeWoodRing() {
		super("RecipeWoodRing", new RecipeRequirement(Stick.class, 2));
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Class craftedItemKind() {
		return WoodRing.class;
	}

}