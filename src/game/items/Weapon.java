package game.items;

public abstract class Weapon extends Equipment {
	
	public Weapon(String name) {
		super(name);
	}
	
	public abstract int
	range();
	
}
