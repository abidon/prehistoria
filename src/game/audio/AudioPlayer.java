package game.audio;

import java.util.HashMap;
import java.util.Map.Entry;

import maryb.player.Player;
import maryb.player.PlayerEventListener;

/**
 * Classe dédiée au playback audio. Prend en charge les fichiers MP3 uniquement.
 * @author Aurelien Bidon
 * @version 1.0
 */
public class AudioPlayer {
	
	/**
	 * Classe dédiée au playback d'un (et un seul) fichier audio.
	 * @author Aurelien Bidon
	 */
	private static class Track implements PlayerEventListener
	{
		/**
		 * On y ajoute juste un attribut permettant de jouer une musique en boucle,
		 * fonctionnalité n'existant pas la bibliothèque utilisée.
		 */
		private boolean mustLoop;
		
		/**
		 * Contient l'instance de classe player qui gère le playback pour nous.
		 */
		private Player p;
		
		/**
		 * Constructeur
		 * @param filepath chemin d'accès du fichier
		 */
		public Track(final String filepath)
		{
			super();
			
			this.p = new Player();
			this.p.setSourceLocation(filepath);
			this.p.setCurrentVolume(1.0f);
			this.p.setListener(this);
			
			this.mustLoop = false;
		}
		
		/**
		 * On n'implemente pas cette méthode.
		 */
		@Override
		public void buffer() {
		}
		
		/**
		 * Une fois la lecture du morceau terminée, on la relance si spécifiée. 
		 */
		@Override
		public void endOfMedia() {
			if (this.mustLoop)
			{
				this.p.play();
			}
		}
		
		/**
		 * Mettre la lecture en pause.
		 */
		public void
		pause()
		{
			this.p.pause();
		}
		
		/**
		 * Lance la lecture asynchrone de la source, sans boucle.
		 */
		public void
		play()
		{
			this.mustLoop = false;
			this.p.play();
		}
		
		/**
		 * Lance la lecture asynchrone de la musique, avec bouclage optionnel.
		 * @param loop true si le flux audio doit boucler.
		 */
		public void
		play(boolean loop)
		{
			this.mustLoop = loop;
			this.p.play();
		}
		
		/**
		 * Lance la lecture synchrone (la méthode retourne une fois le morceau fini).
		 */
		public void
		playSync()
		{
			this.mustLoop = false;
			try {
				this.p.playSync();
			} catch (InterruptedException e) { }
		}
		
		/**
		 * On n'implemente pas cette méthode.
		 */
		@Override
		public void stateChanged() {
		}
		
		/**
		 * Arrête la lecture de la source audio.
		 */
		public void
		stop()
		{
			this.p.stop();
		}
	}
	
	/**
	 * Un serial, utilisé comme préfixe lors des identifiants de sources identiques.
	 */
	private static int serial = 0;
	/**
	 * Le HashMap des sources audios, accessibles via un identifiant litéral.
	 * @see Track
	 */
	private static HashMap<String, Track> sources;
	
	/**
	 * Initialisation de l'AudioPlayer.
	 * @see AudioPlayer
	 */
	static
	{
		sources = new HashMap<String, Track>();
	}
	
	/**
	 * Crée une source audio
	 * @param id L'identifiant litéral voulu de la source
	 * @param filepath Le chemin vers la ressource audio à lire.
	 * @return Retourne l'identifiant final, pouvant etre modifié par le système, si celui-ci existe deja.
	 */
	public static String
	createSource(final String id, final String filepath)
	{
		Track t = new Track(filepath);
		
		String finalId = id;
		
		while (sources.containsKey(finalId))
		{
			finalId += serial++;
		}
		
		sources.put(finalId, t);
		
		return finalId;
	}
	
	/**
	 * Supprime une source audio
	 * @param id l'identifiant de la source à supprimer
	 */
	public static void
	deleteSource(final String id)
	{
		if (sources.containsKey(id))
			sources.remove(id);
	}
	
	/**
	 * Met en pause une source audio
	 * @param id
	 */
	public static void
	pause(final String id)
	{
		if (sources.containsKey(id))
		{
			sources.get(id).pause();
		}
	}
	
	/**
	 * Lit une source audio, de manière asynchone (threadée).
	 * @param id l'identifiant de la source.
	 */
	public static void
	play(final String id)
	{
		if (sources.containsKey(id))
		{
			sources.get(id).play();
		}
	}
	
	/**
	 * Lit une source audio, de manière asynchrone (threadée), avec bouclage optionnel.
	 * @param id l'identifiant de la source.
	 * @param loop true si bouclage de la musique
	 */
	public static void
	play(final String id, boolean loop)
	{
		if (sources.containsKey(id))
		{
			sources.get(id).play(loop);
		}
	}
	
	/**
	 * Lit une source audio, de manière synchrone (non threadée).
	 * @param id l'identifiant de la source.
	 */
	public static void
	playSync(final String id)
	{
		if (sources.containsKey(id))
		{
			sources.get(id).playSync();
		}
	}
	
	/**
	 * Arrête la lecture de la source audio.
	 * @param id l'identifiant de la source
	 */
	public static void
	stop(final String id)
	{
		if (sources.containsKey(id))
		{
			sources.get(id).stop();
		}
	}
	
	/**
	 * Arrete la lecture de tous les morceaux
	 */
	public static void
	stopAll()
	{
		for (Entry<String, Track> source : sources.entrySet())
		{
			source.getValue().stop();
		}
	}
}
