package game.characters;

import game.environment.Event;
import game.environment.Loot;
import game.environment.Map;
import game.environment.Renderer;
import game.environment.Warp;
import game.items.InventorySet;
import game.items.Item;
import game.lang.MessageRepository;
import game.programs.Game;
import game.util.IntPair;
import game.views.GameOverView;
import game.views.GameView;

import com.google.gson.annotations.Expose;
import com.googlecode.lanterna.screen.ScreenCharacterStyle;
import com.googlecode.lanterna.terminal.Terminal.Color;

/**
 * Classe abstraite d√©finissant les propri√©t√©s propres aux personnages jouables
 * @author Emeric de Peretti
 */
public abstract class PlayableCharacter extends Character {
	/**
	 * Le renderer d√©finit la mani√®re dont l'entit√© est rendue a l'√©cran.
	 * @author Aurelien Bidon
	 */
	protected static final Renderer renderer = new Renderer() {

		@Override
		protected Color bgcolor() {
			return Color.WHITE;
		}

		@Override
		protected Color fgcolor() {
			return Color.BLUE;
		}

		@Override
		protected String str() {
			return "Me";
		}

		@Override
		protected ScreenCharacterStyle style() {
			return ScreenCharacterStyle.Bold;
		}
		
	};
	/**
	 * La quantité d'experience courante du personnage
	 */
	@Expose
	private int experience;
	
	/**
	 * En plus d'avoir des objets equippés, le joueur peut avoir un inventaire contenant des ressources pour crafter des objets,
	 * des potions, des armes de rechange etc...
	 */
	@Expose
	private InventorySet inventory;
	
	/**
	 * Constructeur
	 * @param name le nom du personnage
	 */
	public
	PlayableCharacter(String name)
	{
		super(name);
		
		this.experience = 0;
		this.setCurrentHP(this.maxHP());
		
		this.inventory = new InventorySet();
	}
	
	public void activateEvent() {
		Event ev = currentMap().eventLayer().get(currentCoords());
		if (ev instanceof Loot)
		{
			Loot loot = (Loot)ev;
			if (inventorySet().totalWeight() + loot.weight() < strength()*10)
			{
				Item i = loot.pick();
				inventorySet().addItem(i);
				if (Game.instance.currentView() instanceof GameView)
				{
					((GameView)Game.instance.currentView()).addEventMessage(String.format(MessageRepository.messageForId("pickedItem_x"), i.name()));
				}
			}
		} else if (ev instanceof Warp)
		{
			Warp warp = (Warp)ev;
			this.setMap(warp.warp().first(), warp.warp().second());
		}
	}
	
	/**
	 * Gain d'experience
	 * @param amount la quantit√© d'experience gagn√©e
	 * @return true si le personnage a mont√© de niveau
	 * @author Aurelien Bidon
	 */
	public boolean
	addExperience(int amount)
	{
		int level = this.level();
		this.experience += amount;
		if (Game.instance.currentView() instanceof GameView)
		{
			((GameView)Game.instance.currentView()).addEventMessage(String.format(MessageRepository.messageForId("x_gain_y_expPoint"), this.name(), amount));
		}
		if (this.level() > level)
		{
			if (Game.instance.currentView() instanceof GameView)
			{
				((GameView)Game.instance.currentView()).addEventMessage(String.format(MessageRepository.messageForId("x_levelupToLevel_y"), this.name(), this.level()));
			}
			return true;
		}
		return false;
	}
	
	public abstract String
	className();
	
	/**
	 * Quantit√© d'experience courante
	 * @return qte d'experience courante
	 */
	public int
	experience()
	{
		return this.experience;
	}
	
	/**
	 * Inventaire du joueur 
	 * @return l'inventaire courant du joueur
	 */
	public InventorySet
	inventorySet()
	{
		return this.inventory;
	}

	/**
	 * @author Aurelien Bidon
	 */
	@Override
	public int
	level()
	{
		int level;
		final int baseXp = 3;
		final double coef = 1.8; 
		
		for (level = 0; level < 100; level++)
		{
			if (this.experience() > ((int) (baseXp + Math.pow(level, coef))))
				continue;
			else break;
		}
		return level;
	}
	
	@Override
	public boolean
	moveDown()
	{
		boolean ret = super.moveDown();
		if (ret == true && currentMap().eventLayer().get(currentCoords()) != null) activateEvent();
		return ret;
	}
	
	@Override
	public boolean
	moveLeft()
	{
		boolean ret = super.moveLeft();
		if (ret == true && currentMap().eventLayer().get(currentCoords()) != null) activateEvent();
		return ret;
	}
	
	@Override
	public boolean
	moveRight()
	{
		boolean ret = super.moveRight();
		if (ret == true && currentMap().eventLayer().get(currentCoords()) != null) activateEvent();
		return ret;
	}
	
	@Override
	public boolean
	moveUp()
	{
		boolean ret = super.moveUp();
		if (ret == true && currentMap().eventLayer().get(currentCoords()) != null) activateEvent();
		return ret;
	}
	
	@Override
	public Renderer
	renderer()
	{
		return PlayableCharacter.renderer;
	}
	
	@Override
	public void
	setMap(Map m, IntPair coords)
	{
		super.setMap(m, coords);
		
		currentMap().mapSet();
	}
	
	@Override
	public int
	takeDamage(int amount, Character attacker)
	{
		int ret = super.takeDamage(amount, attacker);
		
		if (this.currentHP() <= 0)
		{
			// Game over ! :'(
			Game.instance.setView(new GameOverView());
		}
		
		return ret;
	}
	
}
