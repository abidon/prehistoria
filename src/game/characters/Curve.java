package game.characters;

public class Curve {
	
	private int base;
	private float power;
	
	public Curve(int base, float power)
	{
		this.base = base;
		this.power = power;
	}
	
	/**
	 * Définit un nombre de points pour un niveau
	 * @param level le niveau du personnage
	 * @return le nombre de point pour l'attribut en fonction du niveau
	 */
	public int
	amountForLevel(int level)
	{
		return (int)(base + Math.pow(level, power));
	}
}
