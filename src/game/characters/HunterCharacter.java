package game.characters;

import game.environment.Renderer;
import game.lang.MessageRepository;

/**
 * Une classe concrete de personnage joueur. Un chasseur dans le cas present
 * @author Emeric de Peretti
 */
public class HunterCharacter extends PlayableCharacter {
	
	private static String classNameMsgId = "HunterClassName"; 
	
	/**
	 * Constructeur
	 * @param name le nom du personnage
	 */
	public
	HunterCharacter(String name) {
		super(MessageRepository.messageForId(classNameMsgId) + " " + name);
	}

	@Override
	public String className() {
		return MessageRepository.messageForId(classNameMsgId);
	}
	
	@Override
	public int
	dexterity() {
		Curve curve = new Curve(4, 1.1f);
		return curve.amountForLevel(level()) + equipmentSet().dexterityBonus();
	}
	
	@Override
	public int
	maxHP() {
		Curve lifeCurve = new Curve(20, 1.2f);
		return lifeCurve.amountForLevel(level())+equipmentSet().hpBonus();
	}

	@Override
	public Renderer
	renderer()
	{
		return renderer;
	}
	
	@Override
	public int
	resistance() {
		Curve curve = new Curve(3, 1.1f);
		return curve.amountForLevel(level()) + equipmentSet().resistanceBonus();
	}

	@Override
	public int
	strength() {
		Curve curve = new Curve(5, 1.1f);
		return curve.amountForLevel(level()) + equipmentSet().strengthBonus();
	}

	@Override
	public int experienceGain() {
		return 1;
	}

}
