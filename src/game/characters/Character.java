package game.characters;

import game.environment.Map;
import game.environment.Renderer;
import game.items.EquipmentSet;
import game.lang.MessageRepository;
import game.programs.Game;
import game.util.IntPair;
import game.util.Pathfinder;
import game.views.GameView;

import java.util.ArrayList;
import java.util.Collections;

import com.google.gson.annotations.Expose;

/**
 * Modèle abstrait d'un personnage.
 * @author Aurelien Bidon
 * @author Emeric de Peretti
 */
public abstract class Character implements Comparable<Character>{
	
	public static final int APRequiredToAttack = 3;
	/**
	 * Une constance définissant le nombre de points requis pour se déplacer.
	 */
	public static final int APRequiredToMove = 2;
	/**
	 * Une constante définissant le maximum de points d'action d'un personnage.
	 */
	public static final int MAX_AP = 7;
	/**
	 * Le nombre de points d'actions restant au personnage. 
	 */
	@Expose
	private int currentAP;
	/**
	 * Position du personnage dans le monde
	 */
	@Expose
	private IntPair currentCoords;
	/**
	 * Les points de vie restant au personnage.
	 */
	@Expose
	private int currentHP;
	/**
	 * Carte sur laquelle est le personnage
	 */
	private Map currentMap;
	/**
	 * L'ensemble des objets équipés par le personnage.
	 */
	@Expose
	private final EquipmentSet equipmentSet;
	
	/**
	 * Le nom du personnage.
	 */
	@Expose
	private final String name;
	
	/**
	 * Constructeur
	 * @param name Le nom du personnage
	 */
	public Character(String name) {
		super();
		
		this.name = name;
		this.currentAP = MAX_AP;
		this.currentHP = 0;
		this.equipmentSet = new EquipmentSet();
	}
	
	/**
	 * Directive d'attaque d'un personnage
	 * @param target la cible
	 * @return true si la cible a été attaquée, false si la cible était hors de portée ou alliée.
	 */
	public boolean
	attack(Character target)
	{
		if (target != null && this.currentAP >= APRequiredToAttack && isInAttackRange(target))
		{
			//! TODO prendre en compte le hitChance
			target.takeDamage(attackDamage(), this);
			this.currentAP -= APRequiredToAttack;

			return true;
		}
		
		return false;
	}
	
	/**
	 * Les dommages infligés par le personnage
	 * @return le nombre de points infligés
	 */
	public int
	attackDamage()
	{
		return rawDamage() + equipmentSet().rawDamageBonus();
	}
	
	/**
	 * Calcul la range du personnage, en fonction de son equipement 
	 * @return la range du personnage (minimum 1)
	 */
	private int attackRange() {
		if (this.equipmentSet() == null) return 1;
		if (this.equipmentSet().weapon() == null) return 1;
		return this.equipmentSet().weapon().range();
	}
	
	/**
	 * Position du personnage
	 * @return coordonnées
	 */
	public IntPair
	currentCoords()
	{
		return this.currentCoords;
	}
	
	/**
	 * La vie courante du personnage
	 * @return La vie courante du personnage
	 */
	public int
	currentHP()
	{
		return this.currentHP;
	}
	
	/**
	 * La map ou est actuellement placé l'entité
	 * @return map sur laquelle l'entité evolue 
	 */
	public Map
	currentMap()
	{
		return this.currentMap;
	}
	
	/**
	 * Le niveau d'adresse du personnage. Influe sur l'esquive, l'initiative et la précision.
	 * @return Le niveau d'adresse
	 * @see entreprise
	 * @see escapeChance
	 * @see hitChance  
	 */
	public abstract int
	dexterity();
	
	/**
	 * L'initiative du personnage. Définit son abilité à entreprendre des actions avant ses adversaires.
	 * @return Le niveau d'initiative
	 */
	public int
	entreprise()
	{
		int rolls = this.dexterity() / 3;
		int add = this.dexterity() % 3;
		int entreprise = 0;
		for (int i = 0; i < rolls; i++)
		{
			entreprise += (int)Math.random()%6;
		}
		entreprise += add;
		
		return entreprise  + equipmentSet().entrepriseBonus();
	}
	
	/**
	 * L'ensemble des objets équipés par l'entitée
	 * @return un equipment set comportant les objets équipés par le joueur
	 */
	public EquipmentSet
	equipmentSet()
	{
		return this.equipmentSet;
	}
	
	/**
	 * L'esquive du personnage. Définit son abilité à échapper aux attaques adverses. 
	 * @return La probabilité d'esquive
	 */
	public int
	escapeChance()
	{
		int rolls = this.dexterity() / 6;
		int add = this.dexterity() % 3;
		int escape = 0;
		for (int i = 0; i < rolls; i++)
		{
			escape += (int)Math.random()%4;
		}
		escape += add;
		
		return escape + equipmentSet().escapeChanceBonus();
	}
	
	public abstract int experienceGain();
	
	/**
	 * La résistance totale du personnage, inclue sa résistance naturelle et ses bonus d'équipements.
	 * Elle définit combien de points de dommages seront contrés lors des attaques ennemies.
	 * @return Les dégats déviés par l'armure
	 */
	public int
	guardEfficiency()
	{
		return rawResist() + equipmentSet().guardEfficiencyBonus();
	}
	
	/**
	 * L'esquive du personnage. Définit son abilité à toucher ses adversaires lors d'attaques. 
	 * @return La probabilité d'esquive
	 */
	public int
	hitChance()
	{
		int rolls = this.dexterity() / 4;
		int add = this.dexterity() % 4;
		int hitChance = 0;
		for (int i = 0; i < rolls; i++)
		{
			hitChance += (int)Math.random()%3;
		}
		hitChance += add;
		
		return hitChance + equipmentSet().hitChanceBonus();
	}
	
	public ArrayList<Character>
	hostileCharactersInRange()
	{
		ArrayList<Character> list = new ArrayList<Character>();
		
		for (Character c : currentMap().characters())
		{
			// On teste si le personnage n'est pas le personnage courant
			if (c == this) continue;
			if (c instanceof NonPlayableCharacter)
			{
				NonPlayableCharacter npc = (NonPlayableCharacter)c;
				if (npc.isHostile() && isInAttackRange(npc))
				{
					list.add(c);
				}
			}
		}
		
		return list;
	}
	
	/**
	 * Permet de vérifier qu'un adversaire est dans la zone de d'attaque.
	 * @param target l'adversaire en question
	 * @return true si l'adversaire est attaquable, false si non
	 */
	public boolean
	isInAttackRange(Character target)
	{
		if (target == null) return false;
		
		Pathfinder p = new Pathfinder(this.currentMap(), this.currentCoords(), target.currentCoords(), true);
		while (p.isComputing());
		if (p.path().size() <= attackRange())
		{
			return true;
		}
		
		return false;
	}
	
	/**
	 * Retourne le niveau du personnage. Influe sur les courbes de vie, d'adresse, d'armure, et de force.
	 * @return le niveau du personnage
	 * @see dexterity
	 * @see maxHP
	 * @see resistance
	 * @see strength
	 */
	public abstract int
	level();
	
	/**
	 * Le maximum de points de vie que le joueur peut avoir.
	 * @return Le maximum de points de vie du joueur.
	 */
	public abstract int
	maxHP();
	
	/**
	 * Déplace le personnage d'une case vers le bas en échange de 2 points d'action.
	 */
	public boolean
	moveDown()
	{
		IntPair c = new IntPair(this.currentCoords.first(), this.currentCoords.second()+1);
		if (this.currentMap.canWalkOnTile(c))
		{
			int cost = APRequiredToMove + this.currentMap.tileCost(c);
			if (this.currentAP - cost < 0) return false;
			this.currentCoords = c;
			this.currentAP -= cost;
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * Déplace le personnage d'une case vers la gauche en échange de 2 points d'action.
	 */
	public boolean
	moveLeft()
	{
		IntPair c = new IntPair(this.currentCoords.first()-1, this.currentCoords.second());
		if (this.currentMap.canWalkOnTile(c))
		{
			int cost = APRequiredToMove + this.currentMap.tileCost(c);
			if (this.currentAP - cost < 0) return false;
			this.currentCoords = c;
			this.currentAP -= cost;
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * Déplace le personnage d'une case vers la droite en échange de 2 points d'action.
	 */
	public boolean
	moveRight()
	{
		IntPair c = new IntPair(this.currentCoords.first()+1, this.currentCoords.second());
		if (this.currentMap.canWalkOnTile(c))
		{
			int cost = APRequiredToMove + this.currentMap.tileCost(c);
			if (this.currentAP - cost < 0) return false;
			this.currentCoords = c;
			this.currentAP -= cost;
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * Déplace le personnage d'une case vers le haut en échange de 2 points d'action.
	 */
	public boolean
	moveUp()
	{
		IntPair c = new IntPair(this.currentCoords.first(), this.currentCoords.second()-1);
		if (this.currentMap.canWalkOnTile(c))
		{
			int cost = APRequiredToMove + this.currentMap.tileCost(c);
			if (this.currentAP - cost < 0) return false;
			this.currentCoords = c;
			this.currentAP -= cost;
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * Le nom du personnage.
	 * @return une chaine de caractère contenant le nom du personnage
	 */
	public String
	name()
	{
		return this.name;
	}
	
	/**
	 * Les dommages bruts sont calculés uniquement a partir de la force naturelle du personnage.
	 * Les bonus d'équipement sont exclus.
	 * @return Les dommages bruts infligés par le personnage.
	 * @see attackDamage
	 */
	public int
	rawDamage()
	{
		int rolls = this.strength() / 3;
		int add = this.strength() % 3;
		int rawDamage = 0;
		for (int i = 0; i < rolls; i++)
		{
			rawDamage += 2 + (int)(Math.random()*5);
		}
		rawDamage += add;
		
		return rawDamage;
	}
	
	/**
	 * La résistance brute est calculée uniquement à partir de la resistance naturelle du personnage.
	 * Les bonus d'équipements sont exclus.
	 * @return La resistance brute du personnage.
	 * @see guardEfficiency
	 */
	public int
	rawResist()
	{
		int rolls = this.resistance() / 3;
		int add = this.resistance() % 3;
		int rawResist = 0;
		for (int i = 0; i < rolls; i++)
		{
			rawResist += (int)(Math.random()*6);
		}
		rawResist += add;
		
		return rawResist;
	}
	
	/**
	 * Le nombre de points d'actions restants au personnage.
	 * @return le nombre de points d'action restant [0; 7]
	 */
	public int
	remainingAP()
	{
		return this.currentAP;
	}
	
	/**
	 * Le renderer, contenant les propri�t�s d'affichage, de l'entit�.
	 * @return le renderer de l'entit�
	 */
	public abstract Renderer
	renderer();
	
	/**
	 * Le niveau de resistance du personnage. Influe sur la resistance totale du personnage et son maximum de vie.
	 * @return Le niveau de resistance
	 * @see guardEfficiency
	 * @see maxHP
	 */
	public abstract int
	resistance();
	
	/**
	 * Permet de modifier le nombre de points d'actions.
	 * @param amount le nouveau nombre de points d'action [0;7]
	 * @see MAX_AP
	 */
	public void
	setCurrentAP(int amount)
	{
		if (amount >= 0 || amount <= 7)
		{
			this.currentAP = amount;
		}
	}
	
	/**
	 * Permet de modifier le nombre de points de vie du personnage.
	 * @param value le nouveau nombre de points de vie [0;maxHP]
	 * @see maxHP
	 */
	public void
	setCurrentHP(Integer value)
	{
		this.currentHP = value;
		if (this.currentHP() < 0) this.currentHP = 0;
		if (this.currentHP() > this.maxHP()) this.currentHP = this.maxHP();
	}
	
	/**
	 * Permet de faire apparaitre (spawn) une entitée
	 * @param m la carte sur laquelle faire apparaitre le joueur
	 */
	public void setMap(Map m) {
		setMap(m, null);
	}
	
	/**
	 * Permet de faire apparaitre (spawn) une entitée sur une carte a une position précise
	 * @param m la carte sur laquelle faire apparaitre le joueur
	 */
	public void setMap(Map m, IntPair coords) {
		if (m == null) return;
		
		if (this.currentMap != null)
		{
			this.currentMap.characters().remove(this);
		}
		
		this.currentMap = m;
		this.currentMap.characters().add(this);
		Collections.sort(this.currentMap.characters());
		this.currentCoords = (coords != null) ? coords : m.spawnPoint();
	}
	
	/**
	 * Le niveau de force du personnage. Influe sur les dommages bruts (donc sur les dommages totaux).
	 * @return Le niveau de resistance
	 * @see attackDamage
	 * @see rawDamage
	 */
	public abstract int
	strength();

	/**
	 * Méthode appelée par la méthode attack(...) d'une instance de Character.
	 * Elle soustrait la resistance aux dommages infligés par l'adversaire,
	 * avant de retirer le resultat aux point de vie du personnage, si l'attaque n'est pas esquivée.
	 * @param amount
	 * @param attacker TODO
	 * @return les dommages retirés, ou -1 si esquivé.
	 * @see esquiveChance
	 * @see guardEfficiency 
	 */
	public int
	takeDamage(int amount, Character attacker)
	{
		amount -= this.guardEfficiency();
		if (amount < 0) amount = 0;
		
		this.setCurrentHP(this.currentHP() - amount);
		
		if (Game.instance.currentView() instanceof GameView)
		{
			((GameView)Game.instance.currentView()).addEventMessage(String.format(MessageRepository.messageForId("x_attacked_y_givinghim_z_damage"), attacker.name(), this.name(), amount));
		}
		
		if (currentHP() <= 0)
		{
			this.currentMap().characters().remove(this);
			if (Game.instance.currentView() instanceof GameView)
				((GameView)Game.instance.currentView()).addEventMessage(String.format(MessageRepository.messageForId("x_killed_y"), attacker.name(), this.name()));
			
			if (attacker instanceof PlayableCharacter)
			{
				PlayableCharacter pattacker = (PlayableCharacter)attacker;
				pattacker.addExperience(experienceGain());
			}
		}
		
		return amount;
	}
	
	@Override
	public int compareTo(Character o) {
		if (this.entreprise() < o.entreprise())
			return -1;
		else if(this.entreprise() > o.entreprise())
			return 1;
		else
			return 0;
	}
}
