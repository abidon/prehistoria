package game.characters;



import game.environment.Loot;

import java.util.ArrayList;

/**
 * Classe abstraite définissant les propriétés d'un personnage non jouable
 * @author Emeric de Peretti
 */
public abstract class NonPlayableCharacter extends Character {
	
	public int probabilityOfLoot;
	
	public NonPlayableCharacter(String name) {
		super(name);
		this.probabilityOfLoot = 3;
	}
	
	@Override
	public ArrayList<Character>
	hostileCharactersInRange()
	{
		ArrayList<Character> list = new ArrayList<Character>();
		
		for (Character c : currentMap().characters())
		{
			// On teste si le personnage n'est pas le personnage courant
			if (c == this) continue;
			if (c instanceof NonPlayableCharacter)
			{
				NonPlayableCharacter npc = (NonPlayableCharacter)c;
				if (npc.isHostile() != this.isHostile() && isInAttackRange(npc))
				{
					list.add(c);
				}
			}
		}
		
		return list;
	}
	
	/**
	 * Tout les NPC ne sont pas hostiles, certains joueront le role de marchands, etc...
	 * Cette fonction permet de dicerner les ennemis des alliés. 
	 * @return true si le personnage est hostile
	 */
	public abstract boolean
	isHostile();
	
	public abstract void
	turn();
	
	public abstract Loot
	loot();
	
	@Override
	public int
	takeDamage(int amount, Character attacker)
	{
		int ret = super.takeDamage(amount, attacker);
		
		if (this.currentHP() <= 0)
		{
			int random;
			random = (int)(Math.random()*probabilityOfLoot);
			if (random == 1)
				this.loot();
		}
		
		return ret;
	}
}
