package game.characters;

import game.environment.Loot;
import game.environment.Renderer;
import game.items.Meat;
import game.items.Stick;
import game.programs.Game;
import game.util.IntPair;
import game.util.Pathfinder;

import java.util.ArrayList;

import com.googlecode.lanterna.screen.ScreenCharacterStyle;
import com.googlecode.lanterna.terminal.Terminal.Color;

public class WolfMonster extends NonPlayableCharacter {

	private static final int FIELD_OF_VIEW = 4;
	private static final int MAX_HP = 10;
	
	protected static final Renderer renderer = new Renderer() {

		@Override
		protected Color bgcolor() {
			return Color.RED;
		}

		@Override
		protected Color fgcolor() {
			return Color.WHITE;
		}

		@Override
		protected String str() {
			return "Wo";
		}

		@Override
		protected ScreenCharacterStyle style() {
			return ScreenCharacterStyle.Bold;
		}
		
	};
	
	public WolfMonster() {
		super("Wolf");
		setCurrentHP(MAX_HP);
	}

	@Override
	public int dexterity() {
		return 1;
	}

	@Override
	public boolean isHostile() {
		return true;
	}

	@Override
	public int level() {
		return 5;
	}

	@Override
	public int maxHP() {
		return MAX_HP;
	}

	@Override
	public Renderer renderer() {
		return renderer;
	}

	@Override
	public int resistance() {
		return 3;
	}

	@Override
	public int strength() {
		return 4;
	}

	@Override
	public void turn() {
		Character playerCharacter = Game.instance.mainPlayer();
		int countTurnWithoutAction = 0;
		while(countTurnWithoutAction < 20){
			if(isInAttackRange(playerCharacter))
			{
				if (attack(playerCharacter))
					countTurnWithoutAction = 0;
				else {
					countTurnWithoutAction ++;
				}
			}

			else
			{
				Pathfinder pathfinder = new Pathfinder(this.currentMap(),this.currentCoords(), playerCharacter.currentCoords(), false);
				while(pathfinder.isComputing());
				ArrayList<IntPair> path = new ArrayList<IntPair>(pathfinder.path());

				if( path==null || path.size()<=1 || path.size()>WolfMonster.FIELD_OF_VIEW ) // on teste avec 1 car on ne veut pas aller sur la meme case que le joueur
				{
					int rand = (int) (Math.abs(Math.random()*3));
					boolean moved = false;
					switch (rand)
					{
						case 0:
						{
							moved = moveUp();
							break;
						}
						
						case 1:
						{
							moved = moveLeft();
							break;
						}
						
						case 2:
						{
							moved = moveDown();
							break;
						}
						
						case 3:
						{
							moved = moveRight();
							break;
						}
						
						default:
						{
							break;
						}
					}
					
					if (!moved)
					{
						countTurnWithoutAction++;
						continue;
					}
				}
				
				int cost = currentMap().tileCost(path.get(0)) + Character.APRequiredToMove;
				if(cost <= this.remainingAP())
				{
					setMap(this.currentMap(), path.get(0));
					setCurrentAP(remainingAP()-cost);
					countTurnWithoutAction = 0;
				}
				else
				{
					countTurnWithoutAction++ ;
					continue;
				}
			}
		}
	}

	@Override
	public int experienceGain() {
		return 1;
	}

	@Override
	public Loot 
	loot() 
	{	
		int randomLoot = (int)Math.random()*2;
		Loot loot;
		if (randomLoot == 0)
			loot = new Loot(new Stick(),Game.instance.mainPlayer().currentMap(),this.currentCoords());
		else
			loot = new Loot(new Meat(), Game.instance.mainPlayer().currentMap(),this.currentCoords());
		return loot;
	}

}
