package game.server;

import game.util.Logger;

import java.net.Socket;

public final class ServerThread extends Thread {
	
	private static long instanceSerializer = 0;
	
	private long instanceSerial;
	private Socket socket;
	
	public
	ServerThread(Socket s)
	{
		super();
		
		this.instanceSerial = ++instanceSerializer;
		this.socket = s;
	}
	
	@SuppressWarnings("static-access")
	@Override
	public void run() {
		Logger.logDateAndTime().logLine(": Starting the server instance #" + this.instanceSerial + ". Client address is \"" + this.socket.getInetAddress() + "\".");
		
	}

}
