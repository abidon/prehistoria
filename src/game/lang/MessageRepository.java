package game.lang;

import game.util.Logger;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class MessageRepository {
	private static HashMap<String, String> cachedMessages;
	private static String locale = null;
	
	/**
	 * Bloc statique, appelé avant le premier appel d'une méthode de la classe.
	 * Utilisé pour la mise en cache des fichier localisés. 
	 */
	static
	{
		MessageRepository.cacheLocalizedStrings(); // met en cache les chaines localisées de la langue courante
	}
	
	/**
	 * Ajoute un message au repository de la langue courante
	 * @param id l'identifiant du message
	 * @param message le contenu du message
	 */
	public static void
	addMessage(String id, String message)
	{
		MessageRepository.cachedMessages.put(id, message);
	}
	
	/**
	 * Les messages en cache
	 * @return un dictionnaire identifiant->message
	 */
	public static HashMap<String, String>
	cachedMessages()
	{
		if (cachedMessages == null)
		{
			cachedMessages = new HashMap<String, String>();
		}
		return cachedMessages;
	}
	
	/**
	 * Cette fonction met en cache les messages contenus dans le fichier data/lang/<gameLocale>.json
	 * @see gameLocale
	 */
	private static void
	cacheLocalizedStrings()
	{
		try {
			FileInputStream fstream = new FileInputStream("data/lang/" + gameLocale() + ".json");
			Gson gson = new Gson();
			Reader reader = new InputStreamReader(fstream, "UTF-8");
			cachedMessages = gson.fromJson(reader, new TypeToken<HashMap<String,String>> () {}.getType());
		} catch (Exception e) {
			Logger.logTime();
			Logger.logLine(": Unable to open " + e.getMessage());
			cachedMessages = new HashMap<String, String>();
		}
	}
	
	/**
	 * Forcer une langue plutot que celle du systeme
	 * @param localeCode le code de la langue
	 */
	public static void
	forceLocale(String localeCode)
	{
		if (localeCode != null)
		{
				MessageRepository.locale = localeCode;
		}
		else
		{
			MessageRepository.locale = null;
		}
		cacheLocalizedStrings();
	}
	
	/**
	 * Le locale courant est par défaut la langue du systeme. Il peut etre altéré via la fonction forceLocale()
	 * @return Le locale courant
	 * @see forceLocale
	 */
	public static String
	gameLocale()
	{
		if (MessageRepository.locale == null) return MessageRepository.systemLocale();
		return MessageRepository.locale;
	}
	
	/**
	 * Récuperer le message lié a l'identifiant 
	 * @param id identifiant du message
	 * @return le message pour l'id, ou un placeholder s'il n'existe pas
	 */
	public static String
	messageForId(String id)
	{
		String msg = cachedMessages.get(id);
		if (msg == null)
		{
			msg = "$(MessageForId:" + id + ")";
		}
		return msg;
	}
	
	/**
	 * Sauve le repository dans un fichier data/lang/<locale>.json
	 */
	public static boolean
	saveToJson()
	{
		return saveToJson(null);
	}
	
	/**
	 * Sauve le repository dans un fichier data/lang/<localeCode>.json
	 * @param localeCode pour sauver le repository sous un autre nom
	 */
	public static boolean
	saveToJson(String localeCode)
	{
		final String finalLocalCode = ((localeCode == null) ? gameLocale() : localeCode);
		try {
			FileOutputStream fstream = new FileOutputStream("data/lang/" + finalLocalCode + ".json");
			String json = toJson();
			fstream.write(json.getBytes("UTF-8"));
			fstream.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return false;
		}
	}
	
	/**
	 * La langue systeme
	 * @return le code de langue systeme
	 */
	public static String
	systemLocale()
	{
		return System.getProperty("user.language");
	}
	
	/**
	 * Transcrit le repository courant sous forme de chaine JSON
	 * @href http://en.wikipedia.org/wiki/JSON
	 * @href http://www.json.org
	 */
	public static String
	toJson()
	{
		Gson gson = new Gson();
		return gson.toJson(MessageRepository.cachedMessages);
	}

}
