package game.environment;
import com.googlecode.lanterna.screen.ScreenCharacterStyle;
import com.googlecode.lanterna.terminal.Terminal.Color;

import game.programs.Game;
import game.util.IntPair;
import game.util.Pair;

public final class Warp extends Event {
	
	private static final Renderer RENDERER = new Renderer()
	{

		@Override
		protected Color bgcolor() {
			return Color.MAGENTA;
		}

		@Override
		protected Color fgcolor() {
			return Color.YELLOW;
		}

		@Override
		protected String str() {
			return "Wp";
		}

		@Override
		protected ScreenCharacterStyle style() {
			return null;
		}
		
	};
	
	private IntPair destinationCoord;
	private Map destinationMap;
	private Class<?> destinationMapClass;
	
	public Warp(Map destinationMap, IntPair destinationCoord)
	{
		super();
		
		this.destinationMap = destinationMap;
		this.destinationCoord = destinationCoord;
		this.destinationMapClass = null;
	}
	
	public Warp(Class<?> destinationMapClass, IntPair destinationCoord)
	{
super();
		
		this.destinationMap = null;
		this.destinationCoord = destinationCoord;
		this.destinationMapClass = destinationMapClass;
	}

	public Pair<Map, IntPair>
	warp()
	{
		if (destinationMap != null)
			return (new Pair<Map, IntPair>(this.destinationMap, this.destinationCoord));
		else if (destinationMapClass != null) {
			try {
				return (new Pair<Map, IntPair>((Map)this.destinationMapClass.newInstance(), this.destinationCoord));
			} catch (Exception e) { }
		}
		return new Pair<Map, IntPair>(Game.instance.mainPlayer().currentMap(), Game.instance.mainPlayer().currentCoords());
	}
	
	public Renderer
	renderer()
	{
		return Warp.RENDERER;
	}
}
