package game.environment;

import com.googlecode.lanterna.screen.ScreenCharacterStyle;
import com.googlecode.lanterna.terminal.Terminal.Color;

public final class WallTile implements Tile {
	
	private static final Renderer renderer = new Renderer() {
		@Override
		protected Color bgcolor() {
			return Color.BLACK;
		}

		@Override
		protected Color fgcolor() {
			return Color.WHITE;
		}

		@Override
		protected String str() {
			return "##";
		}

		@Override
		protected ScreenCharacterStyle style() {
			return ScreenCharacterStyle.Bold;
		}
	};
	
	public WallTile() {
		super();
	}

	@Override
	public int cost() {
		return -1;
	}

	@Override
	public boolean isWalkable() {
		return false;
	}

	@Override
	public void playerOff() {
		// Le player ne pourra jamais marcher sur le mur
	}

	@Override
	public void playerOn() {
		// Le player ne pourra jamais marcher sur le mur
	}

	@Override
	public Renderer renderer() {
		return renderer;
	}

}
