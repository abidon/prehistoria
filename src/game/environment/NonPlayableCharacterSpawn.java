package game.environment;

import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.screen.ScreenCharacterStyle;
import com.googlecode.lanterna.terminal.Terminal.Color;

public class NonPlayableCharacterSpawn extends Event {
	
	private static final Renderer RENDERER = new Renderer()
	{

		@Override
		protected Color bgcolor() {
			return null;
		}

		@Override
		protected Color fgcolor() {
			return null;
		}

		@Override
		protected String str() {
			return null;
		}

		@Override
		protected ScreenCharacterStyle style() {
			return null;
		}
		
		@Override
		public void
		renderOnScreen(Screen s, int atX, int atY)
		{
			// Le spawn point n'est pas affiché a l'écran, on override la methode avec une implementation vide
		}
		
	};
	
	public NonPlayableCharacterSpawn() {
		super();

	}
	
	@Override
	public Renderer renderer() {
		return RENDERER;
	}

}
