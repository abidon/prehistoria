package game.environment;

import game.audio.AudioPlayer;

import com.googlecode.lanterna.screen.ScreenCharacterStyle;
import com.googlecode.lanterna.terminal.Terminal.Color;

public final class GrassTile implements Tile {
	
	private static final Renderer renderer = new Renderer() {
		@Override
		protected Color bgcolor() {
			return Color.GREEN;
		}

		@Override
		protected Color fgcolor() {
			return Color.GREEN;
		}

		@Override
		protected String str() {
			return "gr";
		}

		@Override
		protected ScreenCharacterStyle style() {
			return ScreenCharacterStyle.Underline;
		}
	};
	private static final String sound_id = AudioPlayer.createSource("grassSound", "data/sfx/grass.mp3");
	
	public GrassTile() {
		super(); // new Object()
	}

	@Override
	public int cost() {
		return 0;
	}

	@Override
	public boolean isWalkable() {
		return true;
	}

	@Override
	public void playerOff() {
		
	}

	@Override
	public void playerOn() {
		AudioPlayer.play(sound_id);
	}

	@Override
	public Renderer renderer() {
		return renderer;
	}

}
