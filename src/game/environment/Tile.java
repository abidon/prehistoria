package game.environment;

/**
 * Modèle abstrait d'une case de la Map
 * @author Aurelien Bidon
 * @see Map
 */
public interface Tile {
	
	/**
	 * Retourne le cout additionnel necessaire au franchissement du tile.
	 * Pour le pathfinding.
	 * @return cout necessaire au franchissement du tile
	 */
	public int
	cost();
	
	/**
	 * Definit si le joueur est en mesure de marcher sur cette case.
	 * @return true s'il est possible de marcher sur la case, false si non.
	 */
	public boolean
	isWalkable();
	
	/**
	 * Action lorsque qu'un Character à quitté la case (majoritairement pour le son)
	 */
	public void
	playerOff();
	
	/**
	 * Action lorsque qu'un Character est sur la case (majoritairement pour le son)
	 */
	public void
	playerOn();
	
	public Renderer
	renderer();
	
}
