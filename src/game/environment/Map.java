package game.environment;

import game.characters.Character;
import game.characters.NonPlayableCharacter;
import game.programs.Game;
import game.util.IntPair;
import game.util.Logger;
import java.util.ArrayList;

import com.google.gson.annotations.Expose;

public class Map {
	
	@Expose private ArrayList<Character> characters;
	@Expose private EventLayer eventLayer;
	@Expose private String name;
	@Expose private IntPair size;
	@Expose private TileLayer[] tileLayers;
	@Expose private ArrayList<Class<?>> nonPlayableCharacterList;
	@Expose private  int maxMonster;
	
	public Map(String name, IntPair size, int tileLayerCount)
	{
		super();
		
		this.characters	= new ArrayList<Character>();
		this.eventLayer = new EventLayer(size, this);
		this.name = name;
		this.size = size;
		this.nonPlayableCharacterList = new ArrayList<Class<?>>();
		this.maxMonster = 3;
		
		this.tileLayers = new TileLayer[tileLayerCount];
		for (int i = 0; i < tileLayerCount; i++)
		{
			this.tileLayers[i] = new TileLayer(size, this);
		}
	}
	
	public boolean canWalkOnTile(IntPair intPair) {
		if (intPair.first() < 0 || intPair.second() < 0 ||
			intPair.first() >= this.size.first() || intPair.second() >= this.size().second()) return false;
		
		for (int l = 0; l < this.layersCount(); l++)
		{
			TileLayer t = this.tileLayer(l);
			if (((l == 0) && (t.get(intPair) == null)) || /* si pas de tile en x;y sur le layer le plus bas, on ne peut pas marcher */
					(t.get(intPair) != null && t.get(intPair).isWalkable() == false)) // si il est impossible de marcher sur le tile en x;y, peu importe le layer
				return false;
		}
		
		for (Character c : characters())
		{
			if (c.currentCoords().equals(intPair))
				return false;
		}
		
		return true;
	}
	
	public ArrayList<Character>
	characters()
	{
		return this.characters;
	}
	
	public EventLayer
	eventLayer()
	{
		return this.eventLayer;
	}
	
	@SuppressWarnings({"static-access"})
	public void
	fillLayer(int layerId, Class<?> tileKind)
	{
		if (layerId < 0 || layerId >= this.tileLayers.length || tileKind == null)
		{
			Logger.logDateAndTime().logLine(": Unable to fill layer #" + layerId + " on Map " + this + ".");
			return;
		}
		
		TileLayer layer = this.tileLayers[layerId];
		for (int x = 0; x < size.first(); x++)
		{
			for (int y = 0; y < size.second(); y++)
			{
				try {
					layer.set(new IntPair(x, y), (Tile)tileKind.newInstance());
				} catch (Exception e) {
					Logger.logDateAndTime().logLine(": Unable to instanciate an instance of " + tileKind.getCanonicalName());
					e.printStackTrace();
				}
			}
		}
			
	}
	
	public int
	layersCount()
	{
		return this.tileLayers.length;
	}
	
	public String
	name()
	{
		return this.name;
	}
	
	public IntPair
	size()
	{
		return this.size;
	}

	public IntPair spawnPoint() {
		for (int x = 0; x < this.size().first(); x++)
		{
			for (int y = 0; y < this.size().second(); y++)
			{
				Object obj = this.eventLayer().get(new IntPair(x,y));
				if (obj != null && obj.getClass().equals(PlayableCharacterSpawn.class))
					return new IntPair(x,y);
			}
		}
		
		return null;
	}

	public int
	tileCost(IntPair coords) {
		if (coords.first() < 0 || coords.second() < 0 ||
				coords.first() >= this.size.first() || coords.second() >= this.size().second()) return 1000000;
		
		int cost = 0;
		for (int l = 0; l < this.layersCount(); l++)
		{
			TileLayer t = this.tileLayer(l);
			if (t.get(coords) != null)
				cost = t.get(coords).cost();
		}
		return cost;
	}

	public TileLayer
	tileLayer(int index)
	{
		if (index>=tileLayers.length) return null;
		return this.tileLayers[index];
	}

	public void
	spawnEnemy()
	{
		ArrayList<IntPair> nonPlayableCharacterSpawnCoordList = new ArrayList<IntPair>();
		for(int x=0;x<this.eventLayer().size().first();x++)
		{
			for(int y=0;y<this.eventLayer().size().second();y++)
			{
				Event e = eventLayer.get(new IntPair(x, y));
				if(e!= null && (e instanceof NonPlayableCharacterSpawn))
				{	
					nonPlayableCharacterSpawnCoordList.add(new IntPair(x, y));
				}					
			}
		}
		
		if (Game.instance.mainPlayer().currentMap().characters().size()-1 < maxMonster)
		{
			int randomNumber; 
			randomNumber = (int)(Math.random()*(this.nonPlayableCharacterList.size()));
			NonPlayableCharacter enemy ;
			try {
				enemy = ((NonPlayableCharacter)this.nonPlayableCharacterList.get(randomNumber).newInstance());
				int randomNonPlayableCharacterSpawn;
				randomNonPlayableCharacterSpawn = (int)(Math.random()*(nonPlayableCharacterSpawnCoordList.size()));
				enemy.setMap(this, (nonPlayableCharacterSpawnCoordList.get(randomNonPlayableCharacterSpawn)) );
			} catch (Exception e1) {

			}

		}
	}
	
	// Retourne un boolean en cas de monstre non valide : Boss de jeu.
	public boolean
	setEnemySpawnabled(NonPlayableCharacter enemy)
	{
		this.nonPlayableCharacterList.add(enemy.getClass());
		return true;
	}
	
	public void
	setMaxMonster(int i)
	{
		this.maxMonster = i;
	}
	
	public void
	mapSet()
	{
		
	}
}
