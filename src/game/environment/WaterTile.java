package game.environment;

import game.audio.AudioPlayer;

import com.googlecode.lanterna.screen.ScreenCharacterStyle;
import com.googlecode.lanterna.terminal.Terminal.Color;

public final class WaterTile implements Tile {
	
	private static final Renderer renderer = new Renderer() {
		@Override
		protected Color bgcolor() {
			return Color.BLUE;
		}

		@Override
		protected Color fgcolor() {
			return Color.BLUE;
		}

		@Override
		protected String str() {
			return "wa";
		}

		@Override
		protected ScreenCharacterStyle style() {
			return ScreenCharacterStyle.Underline;
		}
	};
	private static String sound_id = AudioPlayer.createSource("dirtSound", "data/sfx/dirt.mp3");
	
	public WaterTile() {
		super(); // new Object()
	}

	@Override
	public int cost() {
		return 2;
	}

	@Override
	public boolean isWalkable() {
		return true;
	}

	@Override
	public void playerOff() {
		
	}

	@Override
	public void playerOn() {
		AudioPlayer.play(sound_id);
	}

	@Override
	public Renderer renderer() {
		return renderer;
	}

}
