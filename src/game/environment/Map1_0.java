package game.environment;

import game.characters.BearMonster;
import game.characters.TRexMonsterBoss;
import game.characters.TigerMonster;
import game.characters.WolfMonster;
import game.lang.MessageRepository;
import game.util.IntPair;
import game.util.MapTools;

public class Map1_0 extends Map {
	
	public Map1_0()
	{
		super(MessageRepository.messageForId("mapregion_valley") + " (?,?)", new IntPair(44,24), 1);
		this.setMaxMonster(10);
		this.setEnemySpawnabled(new WolfMonster());
		this.setEnemySpawnabled(new TigerMonster());
		this.setEnemySpawnabled(new TigerMonster());
		this.setEnemySpawnabled(new TigerMonster());
		this.setEnemySpawnabled(new BearMonster());
		this.setEnemySpawnabled(new BearMonster());
		this.setEnemySpawnabled(new TRexMonsterBoss());
		this.eventLayer().setPlayerSpawnPoint(new IntPair(21,7));
		
		this.fillLayer(0, GrassTile.class);
		MapTools.placeHorizontalLine(0, size().first()-1, 0, WallTile.class, this, 0);
		MapTools.placeHorizontalLine(0, size().first()-1, size().second()-1, WallTile.class, this, 0);
		MapTools.placeVerticalLine(size().first()-1, 0, size().second()-1, WallTile.class, this, 0);
		
		// Teleportation
		MapTools.placeVerticalWarpLine(0, 1, size().second()-2, this, Map0_0.class, 42);
		
		this.eventLayer().set(new IntPair(2,6), new NonPlayableCharacterSpawn());
		this.eventLayer().set(new IntPair(14,20), new NonPlayableCharacterSpawn());
		this.eventLayer().set(new IntPair(29,16), new NonPlayableCharacterSpawn());
		this.eventLayer().set(new IntPair(37,10), new NonPlayableCharacterSpawn());
		this.eventLayer().set(new IntPair(8,10), new NonPlayableCharacterSpawn());
		this.eventLayer().set(new IntPair(23,20), new NonPlayableCharacterSpawn());
	}
	
	@Override
	public void
	mapSet()
	{
		spawnEnemy();
		spawnEnemy();
		spawnEnemy();
	}
	
}
