package game.environment;

import game.util.IntPair;
import game.util.Logger;

public class EventLayer extends Layer<Event> {

	public EventLayer(IntPair size, Map parent) {
		super(size, parent);
	}
	
	@Override
	public void
	set(IntPair coord, Event content)
	{
		if (coord.first() >= size().first() || coord.second() >= size().second())
		{
			Logger.logDateAndTime();
			Logger.logLine(": Tried to set a " + this.content.entrySet().iterator().next().getValue().getClass().getCanonicalName() + " to the " + this.getClass().getCanonicalName() + " out of bounds");
			return;
		}
		
		if (content == null) {
			this.content.put(coord, content);
			return;
		}
		
		if (content.getClass().equals(PlayableCharacterSpawn.class))
		{
			while (this.parent.spawnPoint() != null)
				this.set(this.parent.spawnPoint(), null);
		}
		
		this.content.put(coord, content);
	}
	
	public void
	setPlayerSpawnPoint(int x, int y)
	{
		this.setPlayerSpawnPoint(new IntPair(x,y));
	}
	
	public void
	setPlayerSpawnPoint(IntPair point)
	{
		while (this.parent.spawnPoint() != null)
			this.set(this.parent.spawnPoint(), null);

		this.set(point, new PlayableCharacterSpawn());
	}

}
