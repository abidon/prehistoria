package game.environment;

import game.characters.WolfMonster;
import game.items.Stick;
import game.lang.MessageRepository;
import game.util.IntPair;

public class MapTutorial extends Map {

	public MapTutorial() 
	{
		super(MessageRepository.messageForId("tutorial") + " (?,?)", new IntPair(44,24), 1);
		this.setMaxMonster(1);
		this.setEnemySpawnabled(new WolfMonster());
		this.eventLayer().setPlayerSpawnPoint(new IntPair(21,7));
		
		this.fillLayer(0, GrassTile.class);
		
		// Eau
		for(int y=0; y<5 ; y++)
		{
			for(int x=0; x<44; x++)
			{
				this.tileLayer(0).set(new IntPair(x,y), new WaterTile());
			}
		}
		
		// Vague Sable
		for (int x=0; x<44; x++)
		{
			if (x%8 >=4)
			{
				this.tileLayer(0).set(new IntPair(x, 4), new SandTile());
			}
		}
		
		// Bande de sable
		for(int y=5; y<=6 ; y++)
		{
			for(int x=0; x<44; x++)
			{
				this.tileLayer(0).set(new IntPair(x,y), new SandTile());
			}
		}
		
		// Vague d'Herbe
		for (int x=0; x<44; x++)
		{
			if (x%8>=2 && x%8<6)
			{
				this.tileLayer(0).set(new IntPair(x, 6), new GrassTile());
			}
		}
		
		this.tileLayer(0).set(new IntPair(19,6), new WallTile());
		this.tileLayer(0).set(new IntPair(19,7), new WallTile());
		this.tileLayer(0).set(new IntPair(19,8), new WallTile());
		this.tileLayer(0).set(new IntPair(19,9), new WallTile());
		this.tileLayer(0).set(new IntPair(20,6), new WallTile());
		this.tileLayer(0).set(new IntPair(21,6), new WallTile());
		this.tileLayer(0).set(new IntPair(22,6), new WallTile());
		this.tileLayer(0).set(new IntPair(23,6), new WallTile());
		this.tileLayer(0).set(new IntPair(23,7), new WallTile());
		this.tileLayer(0).set(new IntPair(23,8), new WallTile());
		this.tileLayer(0).set(new IntPair(23,9), new WallTile());
		
		this.eventLayer().set(new IntPair(23,20), new NonPlayableCharacterSpawn());
		
		new Loot(new Stick(), this, new IntPair(18, 20));
		new Loot(new Stick(), this, new IntPair(24, 20));
		
		this.eventLayer().set(new IntPair(21,23), new Warp(new Map0_0(),new IntPair(1,1)));
	}
	
	@Override
	public void
	mapSet()
	{
		spawnEnemy();
	}

}
