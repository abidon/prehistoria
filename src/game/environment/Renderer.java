package game.environment;

import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.screen.ScreenCharacterStyle;
import com.googlecode.lanterna.terminal.Terminal.Color;

public abstract class Renderer {
	protected abstract Color bgcolor();
	protected abstract Color fgcolor();
	public void renderOnScreen(Screen s, int atX, int atY)
	{
		if (s == null) return;
		
		if (style() == null) s.putString(atX, atY, str(), fgcolor(), bgcolor());
		else s.putString(atX, atY, str(), fgcolor(), bgcolor(), style());
	}
	protected abstract String str();
	
	protected abstract ScreenCharacterStyle style();
	
}
