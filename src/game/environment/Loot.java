package game.environment;
import game.items.Item;
import game.util.IntPair;

import com.googlecode.lanterna.screen.ScreenCharacterStyle;
import com.googlecode.lanterna.terminal.Terminal.Color;

public final class Loot extends Event {
	
	private static final Renderer RENDERER = new Renderer()
	{

		@Override
		protected Color bgcolor() {
			return Color.MAGENTA;
		}

		@Override
		protected Color fgcolor() {
			return Color.YELLOW;
		}

		@Override
		protected String str() {
			return "Lt";
		}

		@Override
		protected ScreenCharacterStyle style() {
			return null;
		}
		
	};
	
	private IntPair position;
	private Map map;
	private Item loot;
	
	public Loot(Item loot, Map map, IntPair position)
	{
		super();
		
		this.loot = loot;
		this.map = map;
		this.position = position;
		this.map.eventLayer().set(position, this);
	}
	
	public Item
	pick()
	{
		this.map.eventLayer().set(position, null);
		return this.loot;
	}
	
	public int
	weight()
	{
		return loot.weight();
	}
	
	public Renderer
	renderer()
	{
		return Loot.RENDERER;
	}

}
