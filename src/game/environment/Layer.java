package game.environment;

import game.util.IntPair;
import game.util.Logger;

import java.util.HashMap;

public class Layer<A> {
	
	protected HashMap<IntPair, A> content;
	protected final Map parent;
	private IntPair size;
	
	public Layer(IntPair size, Map parent) {
		super();
		
		this.parent = parent;
		this.size = new IntPair(size);
		this.content = new HashMap<IntPair, A>(size.first() * size.second());
	}
	
	public A
	get(IntPair coord)
	{
		return this.content.get(coord);
	}
	
	public Map
	parentMap()
	{
		return this.parent;
	}
	
	public void
	remove(A object)
	{
		this.content.remove(object);
	}
	
	public void
	set(IntPair coord, A content)
	{
		if (coord.first() >= size.first() || coord.second() >= size.second())
		{
			Logger.logDateAndTime();
			Logger.logLine(": Tried to set a " + this.content.entrySet().iterator().next().getValue().getClass().getCanonicalName() + " to the " + this.getClass().getCanonicalName() + " out of bounds");
			return;
		}
		
		this.content.put(coord, content);
	}
	
	public IntPair
	size()
	{
		return this.size;
	}

}
