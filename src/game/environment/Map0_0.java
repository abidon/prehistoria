package game.environment;

import game.characters.BearMonster;
import game.characters.TigerMonster;
import game.characters.WolfMonster;
import game.lang.MessageRepository;
import game.util.IntPair;
import game.util.MapTools;

public class Map0_0 extends Map {
	
	public Map0_0()
	{
		super(MessageRepository.messageForId("mapregion_valley") + " (?,?)", new IntPair(44,24), 1);
		this.setMaxMonster(10);
		this.setEnemySpawnabled(new WolfMonster());
		this.setEnemySpawnabled(new WolfMonster());
		this.setEnemySpawnabled(new WolfMonster());
		this.setEnemySpawnabled(new TigerMonster());
		this.setEnemySpawnabled(new TigerMonster());
		this.setEnemySpawnabled(new BearMonster());
		this.eventLayer().setPlayerSpawnPoint(new IntPair(21,7));
		
		this.fillLayer(0, GrassTile.class);
		MapTools.placeHorizontalLine(0, size().first()-1, 0, WallTile.class, this, 0);
		MapTools.placeHorizontalLine(0, size().first()-1, size().second()-1, WallTile.class, this, 0);
		MapTools.placeVerticalLine(0, 0, size().second()-1, WallTile.class, this, 0);
		
		// Teleportation
		MapTools.placeVerticalWarpLine(size().first()-1, 1, size().second()-2, this, Map1_0.class, 1);
		
		this.eventLayer().set(new IntPair(2,6), new NonPlayableCharacterSpawn());
		this.eventLayer().set(new IntPair(14,20), new NonPlayableCharacterSpawn());
		this.eventLayer().set(new IntPair(29,16), new NonPlayableCharacterSpawn());
		this.eventLayer().set(new IntPair(37,10), new NonPlayableCharacterSpawn());
		this.eventLayer().set(new IntPair(8,10), new NonPlayableCharacterSpawn());
		this.eventLayer().set(new IntPair(23,20), new NonPlayableCharacterSpawn());
	}
	
	@Override
	public void
	mapSet()
	{
		spawnEnemy();
		spawnEnemy();
		spawnEnemy();
	}
	
}
