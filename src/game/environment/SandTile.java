package game.environment;

import game.audio.AudioPlayer;

import com.googlecode.lanterna.screen.ScreenCharacterStyle;
import com.googlecode.lanterna.terminal.Terminal.Color;

public final class SandTile implements Tile {
	
	private static final Renderer renderer = new Renderer() {
		@Override
		protected Color bgcolor() {
			return Color.YELLOW;
		}

		@Override
		protected Color fgcolor() {
			return Color.YELLOW;
		}

		@Override
		protected String str() {
			return "sd";
		}

		@Override
		protected ScreenCharacterStyle style() {
			return ScreenCharacterStyle.Underline;
		}
	};
	private static String sound_id = AudioPlayer.createSource("rockSound", "data/sfx/rock.mp3");
	
	public SandTile()  {
		super(); // new Object()
	}

	@Override
	public int cost() {
		return 1;
	}

	@Override
	public boolean isWalkable() {
		return true;
	}

	@Override
	public void playerOff() {
		
	}

	@Override
	public void playerOn() {
		AudioPlayer.play(sound_id);
	}

	@Override
	public Renderer renderer() {
		return renderer;
	}

}
