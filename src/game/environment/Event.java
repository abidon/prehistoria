package game.environment;

public abstract class Event {
	
	public Event()
	{
		super();
	}
	
	public abstract Renderer
	renderer();
	
}
