package game.util;

import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.screen.ScreenCharacterStyle;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.TerminalSize;

public final class FrameratedScreen extends Screen {

	private int fpsLimit = 0;
	private long lastRefresh = System.currentTimeMillis();
	private boolean showFramerate = false;

	public FrameratedScreen(Terminal terminal) {
		super(terminal);
	}

	public FrameratedScreen(Terminal terminal, int terminalWidth, int terminalHeight) {
		super(terminal, terminalWidth, terminalHeight);
	}

	public FrameratedScreen(Terminal terminal, TerminalSize terminalSize) {
		super(terminal, terminalSize);
	}
	
	public int
	framerateLimit()
	{
		return this.fpsLimit;
	}
	
	public boolean
	isFramerateCapped()
	{
		return this.fpsLimit > 0;
	}
	
	@Override
	public void
	refresh()
	{
		long renderTime = System.currentTimeMillis() - this.lastRefresh;
		renderTime = renderTime == 0 ? 1 : renderTime;
		if (this.fpsLimit > 0)
		{
			long framedelay = (long)((1.0f/this.fpsLimit) * 1000);
			
			if (renderTime < framedelay)
			{
				try {
					Thread.sleep(framedelay - renderTime);
				} catch (InterruptedException e) { }
			}

			renderTime = System.currentTimeMillis() - this.lastRefresh;
		}
		
		if (this.showFramerate)
		{
			int fps = (int)(1/(renderTime/1000.f));
			String str = "FPS: " + String.valueOf(fps);
			TerminalSize termSize = this.getTerminalSize();
			this.putString(termSize.getColumns() - str.length(), termSize.getRows()-1, str, Terminal.Color.RED, Terminal.Color.YELLOW, ScreenCharacterStyle.Bold);
		}
		
		super.refresh();
		this.lastRefresh = System.currentTimeMillis();
	}
	
	public void
	setFramerateLimit(int limit)
	{
		if (limit >= 0) this.fpsLimit = limit;
	}
	
	public void
	showFramerate(boolean show)
	{
		this.showFramerate = show;
	}
}
