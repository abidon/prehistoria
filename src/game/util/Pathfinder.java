package game.util;

import game.environment.Map;

import java.util.ArrayList;

public class Pathfinder implements IPathfinderDelegate {
	private boolean isComputing;
	private ArrayList<IntPair> path;
	
	public Pathfinder(Map m, IntPair origin, IntPair destination, boolean useDiagonals)
	{
		isComputing = true;
		path = null;
		new DelayedPathfinder(m, origin, destination, this, useDiagonals);
	}

	public boolean isComputing() {
		return isComputing;
	}
	
	public ArrayList<IntPair>
	path()
	{
		return path;
	}

	@Override
	public void pathFound(DelayedPathfinder finder, ArrayList<IntPair> path) {
		this.path = path;
		this.isComputing = false;
	}
}
