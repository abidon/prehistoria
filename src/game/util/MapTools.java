package game.util;

import game.environment.EventLayer;
import game.environment.Map;
import game.environment.Tile;
import game.environment.TileLayer;
import game.environment.Warp;

public class MapTools {
	public static void
	placeHorizontalLine(int x1, int x2, int y, Class<?> tileClass, Map map, int tileLayer)
	{
		if (!instanceOfTile(tileClass)) return;
		
		TileLayer t = map.tileLayer(tileLayer);
		for (int xi = x1; xi <= x2; xi++)
		{
			try {
				t.set(new IntPair(xi, y), (Tile)tileClass.newInstance());
			} catch (Exception e) { }
		}
	}
	
	public static void
	placeVerticalLine(int x, int y1, int y2, Class<?> tileClass, Map map, int tileLayer)
	{
		if (!instanceOfTile(tileClass)) return;
		
		TileLayer t = map.tileLayer(tileLayer);
		for (int yi = y1; yi <= y2; yi++)
		{
			try {
				t.set(new IntPair(x, yi), (Tile)tileClass.newInstance());
			} catch (Exception e) { }
		}
	}
	
	public static void
	placeVerticalWarpLine(int x, int y1, int y2, Map map, Class<?> class1, int destx)
	{
		EventLayer e = map.eventLayer();
		for (int yi = y1; yi <= y2; yi++)
		{
			e.set(new IntPair(x, yi), new Warp(class1, new IntPair(destx, yi)));
		}
	}
	
	private static boolean
	instanceOfTile(Class<?> clazz)
	{
		try {
			if (clazz.newInstance() instanceof Tile)
				return true;
		} catch (Exception e) {
		}
		return false;
	}
}
