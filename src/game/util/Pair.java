package game.util;

/**
 * Un conteneur d'une paire.
 * @author Aurelien Bidon
 *
 * @param <A> type du premier �l�ment
 * @param <B> type du second �l�ment.
 */
public class Pair<A, B> {
	private final A first;
	private final B second;
	
	/**
	 * Constructeur membre � membre
	 * @param first le premier element
	 * @param second le second element
	 * @author Aurelien Bidon
	 */
	public Pair(A first, B second)
	{
		super();
		this.first = first;
		this.second = second;
	}
	
	/**
	 * Constructeur de copie
	 * @param copy la paire a copier
	 * @author Aurelien Bidon
	 */
	public Pair(Pair<A, B> copy)
	{
		this(copy.first, copy.second);
	}
	
	/**
	 * Methode de test d'égalité.
	 * @param other l'objet à comparer
	 * @return true si les objets sont égaux, false si non.
	 * @author Aurelien Bidon
	 */
	@Override
	@SuppressWarnings("unchecked")
	public boolean equals(Object other)
	{
		if ((other instanceof Pair) == false ||
				this.first == null ||
				this.second == null ||
				((Pair<A, B>)other).first == null ||
				((Pair<A, B>)other).second == null)
			return false;
			
		return (this.first.equals(((Pair<A, B>)other).first) &&
				this.second.equals(((Pair<A, B>)other).second));
	}
	
	public boolean
	equals(Pair<A,B> compare)
	{
		return first().equals(compare.first()) && second().equals(compare.second());
	}
	
	/**
	 * Accès au premier élément.
	 * @return le premier élément.
	 * @author Aurelien Bidon
	 */
	public A first()
	{
		return first;
	}
	
	/**
	 * Re-impl�mentation de la m�thode hashCode pour permettre l'utilisation avec HashMap.
	 * @author Aurelien Bidon
	 */
	@Override
	public int hashCode()
	{
		int hashFirst = first != null ? first.hashCode() : 0;
    	int hashSecond = second != null ? second.hashCode() : 0;

    	return (hashFirst + hashSecond) * hashSecond + hashFirst;
	}
	
	/**
	 * Acc�s au second �l�ment.
	 * @return le second �l�ment.
	 * @author Aurelien Bidon
	 */
	public B second()
	{
		return second;
	}
	
	@Override
	public String
	toString()
	{
		return this.getClass().getCanonicalName() + "(" + first() + " , " + second() + ")";
	}
}
