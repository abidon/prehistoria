package game.util;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ConsoleKeyboard {
	
	private static Scanner scanner = new Scanner (System.in);
	
	public static void close ()
	{
		if (scanner == null) return;
		scanner.close ();
	}
	
	public static int
	readInt()
	{
		try
		{
			int i = scanner.nextInt();
			return i;
		} catch (InputMismatchException ime)
		{
			Logger.log(scanner.next() + " n'est pas un entier. Veuillez réessayer : ");
			return readInt();
		}
	}
	
	public static String
	readLine()
	{
		String s = scanner.nextLine();
		return s;
	}
	
	public static String
	readWord()
	{
		String s = scanner.next();
		return s;
	}
	
	public static void
	waitEnterKey()
	{
		System.out.println("Appuyez sur Entrée pour continuer...");
		try {
			System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
