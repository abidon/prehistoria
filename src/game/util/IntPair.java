package game.util;

public class IntPair extends Pair<Integer, Integer> {

	public IntPair(int first, int second) {
		super(first, second);
	}

	public IntPair(IntPair copy) {
		this(copy.first(), copy.second());
	}
	
	public IntPair(Pair<Integer, Integer> copy) {
		this(copy.first(), copy.second());
	}
}
