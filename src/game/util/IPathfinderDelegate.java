package game.util;

import java.util.ArrayList;

public interface IPathfinderDelegate {
	void
	pathFound(DelayedPathfinder finder, ArrayList<IntPair> path);
}
