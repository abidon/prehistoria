package game.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {
	
	private final static Logger instance = new Logger();
	
	public static Logger
	clear()
	{
		logLine("\033[H\033[2J");
		return instance();
	}
	
	public static Logger
	flush()
	{
		System.out.flush();
		return instance();
	}
	
	public static Logger
	instance()
	{
		return instance;
	}
	
	public static Logger
	line()
	{
		System.out.println();
		return instance();
	}
	
	public static Logger
	log(Object obj)
	{
		System.out.print(obj);
		return instance();
	}
	
	public static Logger
	logDate()
	{
		Date now = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("[yy/MM/dd]");
		log(ft.format(now));
		return instance();
	}
	
	public static Logger
	logDateAndTime()
	{
		Date now = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("[yy/MM/dd HH:mm:ss]");
		log(ft.format(now));
		return instance();
	}
	
	public static Logger
	logLine(Object obj)
	{
		System.out.println(obj);
		return instance();
	}
	
	public static Logger
	logTime()
	{
		Date now = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("[HH:mm:ss]");
		log(ft.format(now));
		return instance();
	}
	
}
