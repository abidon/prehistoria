package game.util;

import game.characters.Character;
import game.environment.Map;
import game.environment.Tile;

import java.util.ArrayList;
import java.util.Collections;

public class DelayedPathfinder {
	
	private static class Node
	{

		private IntPair coords;
		private boolean marked;
		public final int ownCost;
		private Node parent;
		private double totalCost;
		public boolean visited;
		
		public Node(IntPair coords, int ownCost)
		{
			this.coords = coords;
			this.ownCost = ownCost;
			this.totalCost = Double.POSITIVE_INFINITY;
		}
		
		private boolean
		isMarked() {
			return this.marked;
		}
		
		public void
		mark()
		{
			this.marked = true;
		}
		
		public Node
		parent()
		{
			return this.parent;
		}
		
		public void
		setParent(Node parent)
		{
			if (!isMarked()) this.parent = parent;
			else Logger.logLine("Le noeud est déjà marqué, impossible de modifier son parent");
		}
		
		public void
		setTotalCost(double cost)
		{
			if (!isMarked()) this.totalCost = cost;
			else Logger.logLine("Le noeud est déjà marqué, impossible de modifier le cout");
		}
		
		public double
		totalCost()
		{
			return this.totalCost;
		}
	}
	
	private static Node
	createObstacleNode(IntPair coords) // noeud déja marqué avec un cout indefini
	{
		Node obstacle = new Node(coords, 0);
		obstacle.setTotalCost(Double.NaN);
		obstacle.visited = true;
		obstacle.mark();
		return obstacle;
	}
	public final IPathfinderDelegate delegate;
	public final IntPair destination;
	private Node[][] dijkstraMap;
	public final Map m;
	
	public final IntPair origin;
	
	public final boolean useDiagonals;
	
	public
	DelayedPathfinder(Map m, IntPair origin, IntPair destination, IPathfinderDelegate delegate)
	{
		this(m, origin, destination, delegate, false);
	}
	
	public
	DelayedPathfinder(Map m, IntPair origin, IntPair destination, IPathfinderDelegate delegate, boolean useDiagonals)
	{
		this.m = m;
		this.origin = origin;
		this.destination = destination;
		this.delegate = delegate;
		this.useDiagonals = useDiagonals;
		
		if (delegate == null) return;
		
		// origin == destination => appeler le callback avec un chemin vide (succes, mais pas besoin de déplacement) 
		if (origin.equals(destination))
		{
			this.delegate.pathFound(this, new ArrayList<IntPair>());
			return;
		}
		
		// origin ou destination n'appartiennent pas a la map => appeler le callback avec null (signe d'erreur) 
		if (origin.first().intValue() < 0 || origin.first().intValue() >= m.size().first().intValue() || destination.first().intValue() < 0 || destination.first().intValue() >= m.size().first().intValue() ||
				origin.second().intValue() < 0 || origin.second().intValue() >= m.size().second().intValue() || destination.second().intValue() < 0 || destination.second().intValue() >= m.size().second().intValue())
			this.delegate.pathFound(this, null);
		
		this.dijkstraMap = new Node[this.m.size().first()][this.m.size().second()];
		this.createDijkstraMap();
		
		this.computePath();
	}

	private void computePath() {
		IntPair nodeCoords = null;
		while ( ((nodeCoords = nextVisitedPoint())) != null )
		{
			Node testingNode = this.dijkstraMap[nodeCoords.first().intValue()][nodeCoords.second().intValue()];
			
			ArrayList<IntPair> surroundingPoints = this.surroundingPoints(nodeCoords);
			for (IntPair surPoint : surroundingPoints)
			{
				Node testedNode = this.dijkstraMap[surPoint.first().intValue()][surPoint.second().intValue()];
				if (testedNode.totalCost() > testingNode.totalCost() + testedNode.ownCost)
				{
					testedNode.setParent(testingNode);
					testedNode.setTotalCost(testingNode.totalCost() + testedNode.ownCost);
				}
				
				testedNode.visited = true;
			}
			
			testingNode.mark();
		}
		
		// Si la destination est marquée, alors un chemin est trouvé
		if (this.dijkstraMap[this.destination.first().intValue()][this.destination.second().intValue()].isMarked())
		{
			this.makePathAndCallback();
		}
		// Si la destination n'est pas marquée, alors elle fait partie d'une autre composante connexe
		// ce qui signifie qu'aucun chemin n'existe entre l'origine et la destination
		else {
			this.delegate.pathFound(this, null);
		}
	}

	private void createDijkstraMap() {
		// on crée l'origine
		this.dijkstraMap[this.origin.first().intValue()][this.origin.second().intValue()] = createOriginNode(origin);
		
		// on crée les autres points
		for (int x = 0; x < this.m.size().first().intValue(); x++)
		{
			for (int y = 0; y < this.m.size().second().intValue(); y++)
			{
				// Si le point x;y est l'origine on passe, ce point spécial ayant déjà été créé
				if (this.origin.first().equals(x) && this.origin.second().equals(y))
					continue;
				
				Tile t = this.m.tileLayer(0).get(new IntPair(x, y));
				if (t.isWalkable() == false)
				{
					this.dijkstraMap[x][y] = createObstacleNode(new IntPair(x, y));
				} else {
					Node node = new Node(new IntPair(x, y), t.cost() + Character.APRequiredToMove);
					this.dijkstraMap[x][y] = node;
				}
			}
		}
	}

	private Node createOriginNode(IntPair originCoords) {
		Node origin = new Node(this.origin, 0);
		origin.setTotalCost(0);
		origin.visited = true;
		// on ne marque pas le point pour que lors du premier tour de calcul, ce soit le seul point a marquer.
		return origin;
	}

	private void makePathAndCallback() {
		ArrayList<IntPair> listCoords = new ArrayList<IntPair>();
		
		listCoords.add(this.dijkstraMap[this.destination.first().intValue()][this.destination.second().intValue()].coords);
		Node parent = this.dijkstraMap[this.destination.first().intValue()][this.destination.second().intValue()];
		while ( ((parent = this.dijkstraMap[parent.coords.first().intValue()][parent.coords.second().intValue()].parent())) != null )
		{
			listCoords.add(this.dijkstraMap[parent.coords.first().intValue()][parent.coords.second().intValue()].coords);
		}
		
		Collections.reverse(listCoords);
		listCoords.remove(0);
		this.delegate.pathFound(this, listCoords);
	}

	private IntPair
	nextVisitedPoint()
	{
		IntPair nextNode = null;
		double cost = Double.POSITIVE_INFINITY;
		for (int x = 0; x < this.m.size().first().intValue(); x++)
		{
			for (int y = 0; y < this.m.size().second().intValue(); y++)
			{
				Node n = this.dijkstraMap[x][y];
				if (true == n.visited && false == n.isMarked() && n.totalCost() <= cost)
				{
					nextNode = new IntPair(x, y);
					cost = n.totalCost();
				}
			}
		}
		return nextNode;
	}
	
	private ArrayList<IntPair> surroundingPoints(IntPair nodeCoords) {
		ArrayList<IntPair> list = new ArrayList<IntPair>();
		int x = nodeCoords.first().intValue();
		int y = nodeCoords.second().intValue();
		
		 // Case Gauche (x-1)
		if (x-1 >= 0 && this.dijkstraMap[x-1][y].isMarked() == false)
			list.add(new IntPair(x-1, y));
		
		// Case Droite (x+1)
		if (x+1 < this.m.size().first().intValue() && this.dijkstraMap[x+1][y].isMarked() == false)
			list.add(new IntPair(x+1, y));
		
		// Case Haut (y-1)
		if (y-1 >= 0 && this.dijkstraMap[x][y-1].isMarked() == false)
			list.add(new IntPair(x, y-1));
		
		// Case Bas (y+1)
		if (y+1 < this.m.size().second().intValue() && this.dijkstraMap[x][y+1].isMarked() == false)
			list.add(new IntPair(x, y+1));
		
		if (this.useDiagonals)
		{
			 // Case Haut Gauche (x-1 et y-1)
			if (x-1 >= 0 && y-1 >= 0 && this.dijkstraMap[x-1][y-1].isMarked() == false)
				list.add(new IntPair(x-1, y-1));
			
			// Case Bas Droite (x+1 et y+1)
			if (x+1 < this.m.size().first().intValue() && y+1 < this.m.size().second().intValue() && this.dijkstraMap[x+1][y+1].isMarked() == false)
				list.add(new IntPair(x+1, y+1));
			
			// Case Haut Droite (x+1 et y-1)
			if (x+1 < this.m.size().first().intValue() && y-1 >= 0 && this.dijkstraMap[x+1][y-1].isMarked() == false)
				list.add(new IntPair(x+1, y-1));
			
			// Case Bas Gauche (x-1 et y+1)
			if (x-1 >= 0 && y+1 < this.m.size().second().intValue() && this.dijkstraMap[x-1][y+1].isMarked() == false)
				list.add(new IntPair(x-1, y+1));
		}
		
		return list;
	}
	
}
