package game.views;

import game.items.Equipment;
import game.items.Item;
import game.lang.MessageRepository;
import game.programs.Game;
import game.util.StringUtil;

import java.util.ArrayList;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.screen.ScreenCharacterStyle;
import com.googlecode.lanterna.terminal.Terminal;

public class InventoryView extends View {

	private int cursorPos;
	private GameView gameView;
	private int messageCase;
	private boolean selectorColumn;


	public InventoryView(GameView gameView) {
		super();
		this.gameView = gameView;
		this.cursorPos = 0;
		this.selectorColumn = false;
		this.messageCase = 0;
	}

	public boolean
	cursorVerification()
	{
		ArrayList<Item> characterInventory = Game.instance.mainPlayer().inventorySet().inventoryList();
		ArrayList<Equipment> characterEquipmentSet = Game.instance.mainPlayer().equipmentSet().equipmentList();

		if(characterInventory.isEmpty() && characterEquipmentSet.isEmpty())
			return true;
		if(characterInventory.isEmpty() && !characterEquipmentSet.isEmpty() && selectorColumn != true)
		{
			cursorPos = 0;
			selectorColumn = true;
		}
		if(!characterInventory.isEmpty() && characterEquipmentSet.isEmpty() && selectorColumn != false)
		{
			cursorPos = 0;
			selectorColumn = false;
		}
		if(((cursorPos >= characterEquipmentSet.size()) && selectorColumn == true) || ((cursorPos >= characterInventory.size()) && selectorColumn == false))
			cursorPos = 0;

		else if((cursorPos <= -1) && selectorColumn == true)
		{
			cursorPos = characterEquipmentSet.size()-1;
		}

		else if((cursorPos <= -1) && selectorColumn == false)
		{
			cursorPos = characterInventory.size()-1;
		}

		return false;
	}

	@Override
	public void processKeyboardEvents(Key k) {
		//ArrayList<Item> characterInventory = Game.instance.mainPlayer().inventorySet().inventoryList();
		ArrayList<Equipment> characterEquipmentSet = Game.instance.mainPlayer().equipmentSet().equipmentList();	
		this.cursorVerification();
		switch (k.getKind())
		{
		case ArrowUp:
		{
			cursorPos--;
			break;
		}

		case ArrowDown:
		{
			cursorPos++;
			break;
		}
		case ArrowLeft:
		{
			selectorColumn = !selectorColumn;
			cursorPos = 0;
			break;
		}

		case ArrowRight:
		{
			selectorColumn = !selectorColumn;
			cursorPos = 0;
			break;
		}

		case Enter:
		{
			break;
		}

		case Escape:
		{
			Game.instance.setView(gameView);
			break;
		}

		case NormalKey:
		{
			switch (k.getCharacter())
			{
			case 'e':
			case 'E':
			{
				if(!selectorColumn && (Game.instance.mainPlayer().inventorySet().selectItem(cursorPos) instanceof Equipment))
				{
					Game.instance.mainPlayer().equipmentSet().setItem(Game.instance.mainPlayer().inventorySet().selectItem(cursorPos));
					this.messageCase = 1;

				}
				else if(selectorColumn)
				{
					Game.instance.mainPlayer().equipmentSet().unsetItem(characterEquipmentSet.get(cursorPos));
					this.messageCase = 2;
				}						
				break;

			}

			case 'd':
			case 'D':
			{
				if(!selectorColumn)
				{
					Game.instance.mainPlayer().inventorySet().deleteItem(cursorPos);
					this.messageCase = 3;
				}
				break;
			}

			case 'c':
			case 'C':
			{
				Game.instance.setView(new CraftingView(this));
				break;
			}

			default:
			{
				break;
			}
			}
		}

		default:
		{	
			break;
		}

		}
		this.cursorVerification();
	}

	@Override
	public void update() {
		ArrayList<Item> characterInventory = Game.instance.mainPlayer().inventorySet().inventoryList();
		ArrayList<Equipment> characterEquipmentSet = Game.instance.mainPlayer().equipmentSet().equipmentList();
		this.screen().clear();
		
		// Bordures et cellules
		this.screen().putString(0, 0, StringUtil.multiplyString("=", this.screenSize().getColumns()), Terminal.Color.YELLOW, Terminal.Color.DEFAULT); // Top
		this.screen().putString(0, this.screenSize().getRows()-1, StringUtil.multiplyString("=", this.screenSize().getColumns()), Terminal.Color.YELLOW, Terminal.Color.DEFAULT); // Bottom
		for (int i = 1; i < this.screenSize().getRows()-1; i++) // Bordures latérales
		{
			String lrBorder = "|" + StringUtil.multiplyString(" ", (this.screenSize().getColumns()-3)/2) + "|" + StringUtil.multiplyString(" ", (this.screenSize().getColumns()-3)/2) + "|"; 
			this.screen().putString(0, i, lrBorder, Terminal.Color.YELLOW, Terminal.Color.DEFAULT);
		}
		this.screen().putString(0, this.screenSize().getRows()-3, StringUtil.multiplyString("=", this.screenSize().getColumns()), Terminal.Color.YELLOW, Terminal.Color.DEFAULT); // Border-top du footer
		
		String inventoryStr = MessageRepository.messageForId("Inventory");
		String equipmentStr = MessageRepository.messageForId("Equipment");
		this.screen().putString(this.screenSize().getColumns()/4 - inventoryStr.length()/2, 1, inventoryStr, Terminal.Color.GREEN, Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold); // Inventory nom colonne
		this.screen().putString((this.screenSize().getColumns()*3)/4 - equipmentStr.length()/2, 1, equipmentStr, Terminal.Color.GREEN, Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold); // Equipment nom colonne
		this.screen().putString(0, 2, StringUtil.multiplyString("=", this.screenSize().getColumns()), Terminal.Color.YELLOW, Terminal.Color.DEFAULT); // Border-bottom du header
		
		// Affichage message d'état
		if (messageCase == 1)
			this.screen().putString(2, this.screenSize().getRows()-4, MessageRepository.messageForId("equip"),Terminal.Color.WHITE, Terminal.Color.BLACK);
		else if(messageCase == 2)
			this.screen().putString(2, this.screenSize().getRows()-4, MessageRepository.messageForId("unequip"),Terminal.Color.WHITE, Terminal.Color.BLACK);
		else if(messageCase == 3)
			this.screen().putString(2, this.screenSize().getRows()-4, MessageRepository.messageForId("deleteitem"),Terminal.Color.WHITE, Terminal.Color.BLACK);

		// Affichage equipement
		for(Equipment itemsEquiped : characterEquipmentSet)
		{
			int index = characterEquipmentSet.indexOf(itemsEquiped);
			String message = ((index == cursorPos) && (selectorColumn == true)) ? "=> " + itemsEquiped.name() : "   " + itemsEquiped.name() ;
			this.screen().putString(this.screenSize().getColumns()/2 + 2 , 4+index*2, message, ((index == cursorPos) && (selectorColumn == true)) ? Terminal.Color.BLUE : Terminal.Color.WHITE, Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold);
		}
		
		// Affichage inventaire
		for(Item itemsInInventory : characterInventory)
		{
			int index = characterInventory.indexOf(itemsInInventory);
			String message = ((index == cursorPos) && (selectorColumn == false)) ? "=> " + itemsInInventory.name() : "   " + itemsInInventory.name() ;
			this.screen().putString(2, 4 + index*2, message, ((index == cursorPos) && (selectorColumn == false)) ? Terminal.Color.BLUE : Terminal.Color.WHITE, Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold);
		}

		// Footer
		this.screen().putString(2,this.screenSize().getRows()-2,MessageRepository.messageForId("footerInventory"), Terminal.Color.WHITE, Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold);

		this.screen().refresh();

	}

}
