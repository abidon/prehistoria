package game.views;

import com.googlecode.lanterna.input.Key;

public final class DummyView extends View {

	public DummyView() {
		super();
	}

	@Override
	public void processKeyboardEvents(Key k) {
		
	}

	@Override
	public void update() {
		this.screen().clear();
		this.screen().refresh();
	}

}
