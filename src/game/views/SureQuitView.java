package game.views;

import game.lang.MessageRepository;
import game.programs.Game;
import game.util.StringUtil;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.screen.ScreenCharacterStyle;
import com.googlecode.lanterna.terminal.Terminal;

public class SureQuitView extends View {

	private final View previousView;
	private boolean quit;
	
	public SureQuitView(View previousView)
	{
		this.previousView = previousView;
		this.quit = false;
	}
	
	@Override
	public void processKeyboardEvents(Key k) {
		switch (k.getKind())
		{
			case ArrowLeft:
			case ArrowRight:
			case ArrowUp:
			case ArrowDown:
			{
				quit = !quit;
				break;
			}
			
			case Enter:
			{
				if (quit)
				{
					Game.instance.setView(new MenuView());
				} else {
					Game.instance.setView(previousView);
				}
				break;
			}
			
			default:
			{
				break;
			}
		}
	}

	@Override
	public void update() {
		this.screen().clear();
		
		// Bordures
		this.screen().putString(0, 3, StringUtil.multiplyString("=", this.screenSize().getColumns()), Terminal.Color.YELLOW, Terminal.Color.DEFAULT); // Top
		this.screen().putString(0, this.screenSize().getRows()-4, StringUtil.multiplyString("=", this.screenSize().getColumns()), Terminal.Color.YELLOW, Terminal.Color.DEFAULT); // Bottom
		
		// Texte
		this.screen().putString((this.screenSize().getColumns() - MessageRepository.messageForId("sureToQuit").length())/2, this.screenSize().getRows()/2 -2, MessageRepository.messageForId("sureToQuit"), Terminal.Color.WHITE, Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold);
		String choice = MessageRepository.messageForId("yes");
		if (!quit)
			choice = MessageRepository.messageForId("no");
		this.screen().putString((this.screenSize().getColumns() - choice.length())/2, this.screenSize().getRows()/2 +2, choice, Terminal.Color.WHITE, Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold);
		
		this.screen().refresh();
	}

}
