package game.views;

import game.lang.MessageRepository;
import game.programs.Game;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.screen.ScreenCharacterStyle;
import com.googlecode.lanterna.terminal.Terminal;

public class GameOverView extends View {

	private final String[] algorushLogo = new String[] {
			 " @@@@@@@@   @@@@@@   @@@@@@@@@@   @@@@@@@@      @@@@@@   @@@  @@@  @@@@@@@@  @@@@@@@ ",
			 "@@@@@@@@@  @@@@@@@@  @@@@@@@@@@@  @@@@@@@@     @@@@@@@@  @@@  @@@  @@@@@@@@  @@@@@@@@",
			 "!@@        @@!  @@@  @@! @@! @@!  @@!          @@!  @@@  @@!  @@@  @@!       @@!  @@@",
			 "!@!        !@!  @!@  !@! !@! !@!  !@!          !@!  @!@  !@!  @!@  !@!       !@!  @!@",
			 "!@! @!@!@  @!@!@!@!  @!! !!@ @!@  @!!!:!       @!@  !@!  @!@  !@!  @!!!:!    @!@!!@! ",
			 "!!! !!@!!  !!!@!!!!  !@!   ! !@!  !!!!!:       !@!  !!!  !@!  !!!  !!!!!:    !!@!@!  ",
			 ":!!   !!:  !!:  !!!  !!:     !!:  !!:          !!:  !!!  :!:  !!:  !!:       !!: :!! ",
			 ":!:   !::  :!:  !:!  :!:     :!:  :!:          :!:  !:!   ::!!:!   :!:       :!:  !:!",
			 " ::: ::::  ::   :::  :::     ::    :: ::::     ::::: ::    ::::     :: ::::  ::   :::",
			 " :: :: :    :   : :   :      :    : :: ::       : :  :      :      : :: ::    :   : :",
	};
	
	private final long VIEW_DURATION = 4 * 1000;
	private final long viewStartTime;
	
	public
	GameOverView() {
		super();
		this.viewStartTime = System.currentTimeMillis();
		//game.audio.AudioPlayer.createSource("menuBgm", "data/music/Battle/Cloak and Dagger.mp3");
		//game.audio.AudioPlayer.play("menuBgm", true);
	}
	
	@Override
	public void
	processKeyboardEvents(Key k)
	{
		
	}
	
	@Override
	public void
	update()
	{
		if (this.screen() == null) return;
		
		if (System.currentTimeMillis() - this.viewStartTime >= VIEW_DURATION)
		{
			Game.instance.setView(new MenuView());
			return;
		}
		
		this.screen().clear();
		
		for (int i = 0; i < algorushLogo.length; i++)
		{
			this.screen().putString( (screenSize().getColumns() - algorushLogo[i].length())/2, ((screenSize().getRows() - algorushLogo.length)/2)+i, algorushLogo[i], Terminal.Color.GREEN, Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold);
		}
		
		String present = MessageRepository.messageForId("game_over_caption");
		this.screen().putString((screenSize().getColumns() - present.length())/2, screenSize().getRows()/2 + algorushLogo.length/2 +1, present, Terminal.Color.MAGENTA, Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold);
		
		this.screen().refresh();
	}

}
