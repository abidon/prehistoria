package game.views;

import game.util.Logger;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.terminal.TerminalSize;

public abstract class View {
	
	private Screen screen = null;
	
	public View() {
		super();
	}
	
	public abstract void
	processKeyboardEvents(Key k);
	
	public void
	removeFromScreen()
	{
		Logger.logTime();
		Logger.logLine(": " +this.getClass().getCanonicalName() + ".removeFromScreen()");
		this.screen = null;
	}
	
	public Screen
	screen()
	{
		return this.screen;
	}
	
	public TerminalSize
	screenSize()
	{
		if (screen == null) return null;
		return this.screen.getTerminalSize();
	}
	
	public void
	showOnScreen(Screen screen)
	{
		Logger.logTime();
		Logger.logLine(": " +this.getClass().getCanonicalName() + ".showOnScreen()");
		this.screen = screen;
	}
	
	public abstract void
	update();

}
