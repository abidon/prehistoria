package game.views;

import game.lang.MessageRepository;
import game.views.actions.Action;
import game.views.actions.CharacterCreationAction;
import game.views.actions.LoadGameAction;
import game.views.actions.OptionsAction;
import game.views.actions.QuitAction;

import java.util.ArrayList;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.screen.ScreenCharacterStyle;
import com.googlecode.lanterna.terminal.Terminal;

public final class MenuView extends View {
	
	private ArrayList<Action> actions;
	
	private int curPos;
	private final String[] gameLogo = new String[] {
			"8888888b.                   888      d8b          888                     d8b         ",
			"888   Y88b                  888      Y8P          888                     Y8P         ",
			"888    888                  888                   888                                 ",
			"888   d88P 888d888  .d88b.  88888b.  888 .d8888b  888888  .d88b.  888d888 888  8888b. ",
			"8888888P\"  888P\"   d8P  Y8b 888 \"88b 888 88K      888    d88\"\"88b 888P\"   888     \"88b",
			"888        888     88888888 888  888 888 \"Y8888b. 888    888  888 888     888 .d888888",
			"888        888     Y8b.     888  888 888      X88 Y88b.  Y88..88P 888     888 888  888",
			"888        888      \"Y8888  888  888 888  88888P'  \"Y888  \"Y88P\"  888     888 \"Y888888",
			"                                                                                      ",
			"Loose yourself into the prehistoric age",
	};
	
	private final String[] trollLogo = new String[] {
			"                           d888888P                   dP dP                           ",
            "                              88                      88 88                           ",
            "                              88    88d888b. .d8888b. 88 88                           ",
            "                              88    88'  `88 88'  `88 88 88                           ",
            "                              88    88       88.  .88 88 88                           ",
            "                              dP    dP       `88888P' dP dP                           ",
            "                                                                                      ",
            "                                             Easter egg? :D                           "
	};
	
	public MenuView() {
		super();
		
		this.curPos = 0;
		
		this.actions = new ArrayList<Action>();
		this.actions.add(new CharacterCreationAction());
		this.actions.add(new LoadGameAction());
		this.actions.add(new OptionsAction());
		this.actions.add(new QuitAction());
	}

	@Override
	public void processKeyboardEvents(Key k) {
		switch (k.getKind())
		{
			case ArrowUp:
			{
				if (--this.curPos < 0) this.curPos = this.actions.size()-1;
				break;
			}
			
			case ArrowDown:
			{
				if (++this.curPos >= this.actions.size()) this.curPos = 0;
				break;
			}
			
			case Enter:
			{
				this.actions.get(curPos).execute();
				break;
			}
			
			default:
			{
				break;
			}
		}
	}

	@Override
	public void update() {
		this.screen().clear();
		
		if (MessageRepository.gameLocale().equals("ee")) // Est-ce que les trolls on laissé un petit Easter Egg?
		{
			for (int i = 0; i < trollLogo.length; i++)
			{
				this.screen().putString( (screenSize().getColumns() - trollLogo[i].length())/2, (screenSize().getRows() - trollLogo.length)/3 + i, trollLogo[i], Terminal.Color.BLUE, Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold);
			}
		} else
		{
			for (int i = 0; i < gameLogo.length; i++)
			{
				this.screen().putString( (screenSize().getColumns() - gameLogo[i].length())/2, (screenSize().getRows() - gameLogo.length)/3 + i, gameLogo[i], Terminal.Color.BLUE, Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold);
			}
		}
		
		for (Action a : actions)
		{
			int index = actions.indexOf(a);
			String message = (index == curPos) ? "=> " + a.message() + " <=" : a.message();
			this.screen().putString(this.screenSize().getColumns()/2 - message.length()/2, (this.screenSize().getRows()*2)/3 + index*2, message, (index == this.curPos) ? Terminal.Color.BLUE : Terminal.Color.WHITE, (index == this.curPos) ? Terminal.Color.WHITE : Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold);
		}
		
		String copyright = MessageRepository.messageForId("copyright_string");
		this.screen().putString((this.screenSize().getColumns() - copyright.length())/2, this.screenSize().getRows()-2, copyright, Terminal.Color.BLUE, Terminal.Color.BLACK, ScreenCharacterStyle.Underline);
		
		this.screen().refresh();
	}

}
