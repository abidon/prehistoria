package game.views;

import game.items.Item;
import game.items.Recipe;
import game.items.RecipeRepository;
import game.lang.MessageRepository;
import game.programs.Game;
import game.util.StringUtil;

import java.util.ArrayList;
import java.util.Map.Entry;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.screen.ScreenCharacterStyle;
import com.googlecode.lanterna.terminal.Terminal;

public class CraftingView extends View {

	private int cursorPos;
	private InventoryView inventoryView;
	private int messageCase;
	private ArrayList<Class<?>> recipes;
	
	public CraftingView(InventoryView inventoryView) {
		this.inventoryView = inventoryView;
		this.cursorPos = 0;
		this.recipes = RecipeRepository.recipeList();
		this.messageCase = 0;
	}

	@Override
	public void processKeyboardEvents(Key k) {
		switch (k.getKind())
		{
			case ArrowUp:
			{
				if (cursorPos <= 0)
					cursorPos = this.recipes.size()-1;
				else
					cursorPos--;
				break;
			}
			case ArrowDown:
			{
				if (cursorPos >= this.recipes.size()-1)
					cursorPos = 0;
				else
					cursorPos++;
				break;
			}
			case Escape:
			{
				Game.instance.setView(inventoryView);
				break;
			}
			
			case NormalKey:
			{	
				switch (k.getCharacter())
				{
					case 'c':
					case 'C':
					{
						try {
							if(((Recipe)recipes.get(cursorPos).newInstance()).hasRequiredResources(Game.instance.mainPlayer()))
							{
								if(((Recipe)recipes.get(cursorPos).newInstance()).craft(Game.instance.mainPlayer()))
									messageCase = 1;
								else
									messageCase = 2;
							}
						} catch (Exception e) {

						}
						break;
					}
				
					default:
					{
						break;
					}
				}
			}
			
			default:
			{
				break;
			}
		}
	}

	@Override
	public void update() {
		this.screen().clear();
		
		// Bordures et cellules
		this.screen().putString(0, 0, StringUtil.multiplyString("=", this.screenSize().getColumns()), Terminal.Color.YELLOW, Terminal.Color.DEFAULT); // Top
		this.screen().putString(0, this.screenSize().getRows()-1, StringUtil.multiplyString("=", this.screenSize().getColumns()), Terminal.Color.YELLOW, Terminal.Color.DEFAULT); // Bottom
		for (int i = 1; i < this.screenSize().getRows()-1; i++) // Bordures latérales
		{
			String lrBorder = "|" + StringUtil.multiplyString(" ", (this.screenSize().getColumns()-3)/2) + "|" + StringUtil.multiplyString(" ", (this.screenSize().getColumns()-3)/2) + "|"; 
			this.screen().putString(0, i, lrBorder, Terminal.Color.YELLOW, Terminal.Color.DEFAULT);
		}
		this.screen().putString(0, this.screenSize().getRows()-3, StringUtil.multiplyString("=", this.screenSize().getColumns()), Terminal.Color.YELLOW, Terminal.Color.DEFAULT); // Border-top du footer
		
		String inventoryStr = MessageRepository.messageForId("Recipe");
		String equipmentStr = MessageRepository.messageForId("Requirements");
		this.screen().putString(this.screenSize().getColumns()/4 - inventoryStr.length()/2, 1, inventoryStr, Terminal.Color.GREEN, Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold); // Inventory nom colonne
		this.screen().putString((this.screenSize().getColumns()*3)/4 - equipmentStr.length()/2, 1, equipmentStr, Terminal.Color.GREEN, Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold); // Equipment nom colonne
		this.screen().putString(0, 2, StringUtil.multiplyString("=", this.screenSize().getColumns()), Terminal.Color.YELLOW, Terminal.Color.DEFAULT); // Border-bottom du header
		
		
		if (messageCase == 1)
			this.screen().putString(2, this.screenSize().getRows()-4, MessageRepository.messageForId("crafted"),Terminal.Color.WHITE, Terminal.Color.BLACK,ScreenCharacterStyle.Bold);
		else if(messageCase == 2)
			this.screen().putString(2, this.screenSize().getRows()-4, MessageRepository.messageForId("notCrafted"),Terminal.Color.WHITE, Terminal.Color.BLACK,ScreenCharacterStyle.Bold);
		
		for(Class<?> c : recipes )
		{
			int index = this.recipes.indexOf(c);
			try {
				String message = (index == cursorPos) ? "=> " + ((Recipe)c.newInstance()).name() : "   "+((Recipe)c.newInstance()).name();
				this.screen().putString(2,4+index*2, message,(index == this.cursorPos) ? Terminal.Color.BLUE : Terminal.Color.WHITE, Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold);
				
				String requirementMessage = "";
				int indexRequirement = 1;
				for (Entry<Class<?>, Integer> requirement : ((Recipe)c.newInstance()).requirements().entrySet())
				{
					
					requirementMessage += ((Item)requirement.getKey().newInstance()).name() + " x" + requirement.getValue().toString();
					if (indexRequirement < ((Recipe)c.newInstance()).requirements().size())
						requirementMessage += ", ";
					indexRequirement++;
				}
				this.screen().putString(2+(this.screenSize().getColumns()/2),4+ index*2, requirementMessage ,Terminal.Color.WHITE, Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		this.screen().putString(2,this.screenSize().getRows()-2,MessageRepository.messageForId("footerCraft"), Terminal.Color.WHITE, Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold);
		
		this.screen().refresh();

	}

}
