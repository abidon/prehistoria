package game.views;

import game.characters.HunterCharacter;
import game.characters.PlayableCharacter;
import game.lang.MessageRepository;
import game.views.actions.NewGameAction;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.screen.ScreenCharacterStyle;
import com.googlecode.lanterna.terminal.Terminal;

public class CharacterCreationView extends View {
	
	private static String[] randomNames = new String[]{"Angol", "Entet", "Omdine", "Vorvbur", "Drapusk", "Hinaq",
		"Snever", "Struath", "Worera", "Oldel", "Zaing", "Essxler", "Aughage", "Raknina", "Orelbe", "Excelias",
		"Lina", "Tagadur", "Kendareth", "Happy"
		};
	private static int MAX_CURSOR = 2;
	private static Class<?>[] playableClasses = new Class<?>[]{HunterCharacter.class};
	
	private int cursorPosition;
	private int choosenClass;
	private String typedCharacterName;
	
	public CharacterCreationView() {
		super();
		this.cursorPosition = 0;
		this.choosenClass = (int)(Math.random() * playableClasses.length);
		typedCharacterName = randomName();
	}
	
	@Override
	public void processKeyboardEvents(Key k) {
		switch (k.getKind())
		{
			case ArrowUp:
			{
				cursorPosition--;
				if (cursorPosition < 0)
					cursorPosition = MAX_CURSOR;
				break;
			}
			
			case ArrowDown:
			{
				cursorPosition++;
				if (cursorPosition > MAX_CURSOR)
					cursorPosition = 0;
				break;
			}
			
			case Backspace:
			{
				typedCharacterName = typedCharacterName.substring(0, typedCharacterName.length() - 1);
				break;
			}
			
			case Enter:
			{
				if (cursorPosition == MAX_CURSOR)
				{
					new NewGameAction(typedCharacterName, playableClasses[choosenClass].getName()).execute();
				}
				break;
			}
			
			case Tab:
			{
				typedCharacterName = randomName();
				break;
			}
			
			case NormalKey:
			{
				if (typedCharacterName.length() < 32)
					typedCharacterName += k.getCharacter();
			}
			
			default:
			{
				break;
			}
		}
	}
	
	public static String
	randomName()
	{
		return randomNames[(int)(Math.random() * randomNames.length)];
	}

	@Override
	public void update() {
		if (this.screen() == null) return;
		
		this.screen().clear();
		
		// Nom du personnage
		this.screen().putString((this.screenSize().getColumns() - MessageRepository.messageForId("CharacterName").length())/2, 10, MessageRepository.messageForId("CharacterName"), Terminal.Color.WHITE, Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold);
		this.screen().putString((this.screenSize().getColumns() - typedCharacterName.length())/2, 12, typedCharacterName, Terminal.Color.BLUE, cursorPosition == 0 ? Terminal.Color.WHITE : Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold);
		this.screen().putString((this.screenSize().getColumns() - MessageRepository.messageForId("CharacterNameRandomInstruction").length())/2, 11, MessageRepository.messageForId("CharacterNameRandomInstruction"), Terminal.Color.WHITE, Terminal.Color.DEFAULT);
		
		// Classe du personnage
		this.screen().putString((this.screenSize().getColumns() - MessageRepository.messageForId("CharacterClass").length())/2, 16, MessageRepository.messageForId("CharacterClass"), Terminal.Color.WHITE, Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold);
		String className = "?";
		try {
			className = ((PlayableCharacter)playableClasses[choosenClass].getConstructor(String.class).newInstance("")).className();
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.screen().putString((this.screenSize().getColumns() - className.length())/2, 18, className, Terminal.Color.BLUE, cursorPosition == 1 ? Terminal.Color.WHITE : Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold);
		
		// Créer
		this.screen().putString((this.screenSize().getColumns() - MessageRepository.messageForId("jumpIntoPrehistoria").length())/2, 22, MessageRepository.messageForId("jumpIntoPrehistoria"), Terminal.Color.BLUE, cursorPosition == 2 ? Terminal.Color.WHITE : Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold);
		
		// Footer
		this.screen().putString((this.screenSize().getColumns() - MessageRepository.messageForId("CharacterNameWarning").length())/2, this.screenSize().getRows()-1, MessageRepository.messageForId("CharacterNameWarning"), Terminal.Color.WHITE, Terminal.Color.DEFAULT);
		
		this.screen().refresh();
	}

}
