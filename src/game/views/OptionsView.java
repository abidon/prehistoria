package game.views;

import game.lang.MessageRepository;
import game.views.actions.Action;
import game.views.actions.BackToMainMenuAction;

import java.io.File;
import java.util.ArrayList;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.screen.ScreenCharacterStyle;
import com.googlecode.lanterna.terminal.Terminal;

public class OptionsView extends View {

	private Action backToMainMenu;
	private int curPos;
	private int languagePos;
	private ArrayList<String> languageTable;
	
	public OptionsView() {
		super();
		this.languageTable = new ArrayList<String>();
		
		File folder = new File("data/lang/");
		File[] files = folder.listFiles();
		for (int i=0 ; i<files.length ; i++)
		{
			if(files[i].isFile())
			{
				if (files[i].getName().toLowerCase().endsWith(".json"))
					this.languageTable.add(files[i].getName().replaceFirst("[.][^.]+$", ""));
			}
		}
		
		this.languagePos = languageTable.indexOf(MessageRepository.gameLocale());
		this.languagePos = (this.languagePos == -1) ? 0 : this.languagePos;
		this.curPos = 0;
		this.backToMainMenu = new BackToMainMenuAction(false);
	}

	public void
	addLanguage(String l18nCode)
	{
		this.languageTable.add(l18nCode);
	}
	
	@Override
	public void processKeyboardEvents(Key k) {
		switch (k.getKind())
		{
			case ArrowUp:
			{
				if (this.curPos == 0)
					this.curPos = 1;
				else
					this.curPos--;
				break;
			}
			
			case ArrowDown:
			{
				if (this.curPos == 1)
					this.curPos = 0;
				else
					this.curPos++;
				break;
			}
			
			case Enter:
			{
				if (this.curPos == 1)
				{
					this.backToMainMenu.execute();
				}
				
				else if (curPos == 0)
				{
					if (this.languagePos == this.languageTable.size())
						this.languagePos = 0;
					else
						this.languagePos++;
					MessageRepository.forceLocale(languageTable.get(languagePos));
				}
				
				break;
				
			}
			
			case ArrowLeft:
			{
				if(this.curPos == 0)
				{
					if (this.languagePos == 0)
						this.languagePos = this.languageTable.size()-1;
					else
						this.languagePos --;
					MessageRepository.forceLocale(languageTable.get(languagePos));
				}
				
				break;
			}
			
			case ArrowRight:
			{
				if(this.curPos == 0)
				{
					if (this.languagePos == this.languageTable.size()-1)
						this.languagePos = 0;
					else
						this.languagePos++;
					MessageRepository.forceLocale(languageTable.get(languagePos));
				}
				break;
			}
			
			default:
			{
				break;
			}
		}
	}

	@Override
	public void update() 
	{
		this.screen().clear();
		
		// Langauge
		this.screen().putString(
				this.screenSize().getColumns()/2 - MessageRepository.messageForId("languageoption").length()/2 - ((curPos == 0) ? 3 : 0),
				3,
				(curPos==0) ? "<= "+MessageRepository.messageForId("languageoption")+" =>" : MessageRepository.messageForId("languageoption"),
				(curPos==0) ? Terminal.Color.BLUE : Terminal.Color.WHITE, 
				(curPos==0) ? Terminal.Color.WHITE : Terminal.Color.DEFAULT,
				ScreenCharacterStyle.Bold);
		
		// Retour au menu
		this.screen().putString(
				this.screenSize().getColumns()/2 - backToMainMenu.message().length()/2 - ((curPos == 2) ? 3 : 0),
				3+this.screenSize().getRows()/3,
				(curPos==1) ? backToMainMenu.message() : backToMainMenu.message(),
				(curPos==1) ? Terminal.Color.BLUE : Terminal.Color.WHITE,
				(curPos==1) ? Terminal.Color.WHITE : Terminal.Color.DEFAULT,
				ScreenCharacterStyle.Bold);
		
		this.screen().refresh();
		
	}

}
