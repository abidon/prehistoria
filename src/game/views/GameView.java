package game.views;

import game.characters.Character;
import game.characters.NonPlayableCharacter;
import game.characters.PlayableCharacter;
import game.environment.Event;
import game.environment.EventLayer;
import game.environment.Map;
import game.environment.Renderer;
import game.environment.Tile;
import game.items.Item;
import game.items.Usable;
import game.lang.MessageRepository;
import game.programs.Game;
import game.util.IntPair;
import game.views.actions.Action;
import game.views.actions.AttackCharacterAction;
import game.views.actions.AttackModeAction;
import game.views.actions.BackToMainMenuAction;
import game.views.actions.EndTurnAction;
import game.views.actions.GoBackToParentMenuAction;
import game.views.actions.InventoryAction;
import game.views.actions.MoveAction;
import game.views.actions.SaveGameAction;
import game.views.actions.UsableAction;
import game.views.actions.UseItemOrSpellAction;

import java.util.ArrayList;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.screen.ScreenCharacterStyle;
import com.googlecode.lanterna.terminal.Terminal;

public class GameView extends View {
	
	public static enum Mode
	{
		Attack("attackMenu"),
		Menu("playerMenu"),
		Move("moveMenu"),
		OtherTurn("otherTurn"),
		Use("useMenu");
		
		public final String messageId;
		
		Mode(String messageId)
		{
			this.messageId = messageId;
		}
		
		public String
		message()
		{
			return MessageRepository.messageForId(messageId);
		}
	}
	
	private Mode currentMode;
	private int cursorPosition;
	private ArrayList<String> eventMessages;
	private int turnCounter;
	
	public GameView() {
		super();
		
		this.currentMode = Mode.Menu;
		this.cursorPosition = 0;
		this.eventMessages = new ArrayList<String>();
		this.turnCounter = 1;
	}
	
	public void addCharacterOnMap(Character c)
	{
		if (charactersOnMap().contains(c) == false)
		{
			charactersOnMap().add(c);
		}
	}

	public void
	addEventMessage(String msg)
	{
		this.eventMessages.add(0, msg);
		while (this.eventMessages.size() > 3)
		{
			this.eventMessages.remove(this.eventMessages.size()-1);
		}
	}

	public ArrayList<Character> charactersOnMap() {
		return Game.instance.mainPlayer().currentMap().characters();
	}
	
	public void
	enableAttack()
	{
		this.currentMode = Mode.Attack;
		this.cursorPosition = 0;
	}
	
	public void
	enableInventory()
	{
		this.cursorPosition = 0;
		Game.instance.setView(new InventoryView(this));
	}

	public void
	enableMovement()
	{
		this.currentMode = Mode.Move;
		this.cursorPosition = 0;
	}
	
	public void
	enableUsable() {
		this.currentMode = Mode.Use;
		this.cursorPosition = 0;
	}
	
	public void
	eventMessagesRendering()
	{
		int i = 0;
		for (String evmsg : this.eventMessages)
		{
			this.screen().putString(2, this.screenSize().getRows()-(4+i), evmsg, Terminal.Color.WHITE, Terminal.Color.BLACK);
			i ++;
		}
	}
	
	public void goBackToParentMenu() {
		if (this.currentMode != Mode.Menu)
		{
			this.currentMode = Mode.Menu;
			this.cursorPosition = 0;
		}
	}

	private ArrayList<Action>
	hudActionList() {
		ArrayList<Action> actions = new ArrayList<Action>();
		
		if (this.currentMode == Mode.Attack)
		{
			ArrayList<Character> list = Game.instance.mainPlayer().hostileCharactersInRange();
			for (Character enemy : list)
			{
				actions.add(new AttackCharacterAction(Game.instance.mainPlayer(), enemy));
			}
			
			actions.add(new GoBackToParentMenuAction());
		} else if (this.currentMode == Mode.Move)
		{
			actions.add(new GoBackToParentMenuAction());
		} else if (this.currentMode == Mode.Menu) {
			actions.add(new MoveAction());
			actions.add(new AttackModeAction());
			actions.add(new UseItemOrSpellAction());
			actions.add(new InventoryAction());
			actions.add(new EndTurnAction());
			actions.add(new SaveGameAction());
			actions.add(new BackToMainMenuAction(true));
		} else if (this.currentMode == Mode.Use)
		{
			for (Item i : Game.instance.mainPlayer().inventorySet().itemsOfKind(Usable.class))
			{
				actions.add(new UsableAction((Usable)i));
			}
			actions.add(new GoBackToParentMenuAction());
		}
		return actions;
	}
	
	private void
	hudRendering()
	{
		ArrayList<Action> actions = hudActionList();
		
		// Menu header
		this.screen().putString(this.screenSize().getColumns()-29, 0, "                             ", Terminal.Color.YELLOW, Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold, ScreenCharacterStyle.Underline);
		this.screen().putString(this.screenSize().getColumns()-29, 1, "| "+this.currentMode.message()+"                             ", Terminal.Color.YELLOW, Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold, ScreenCharacterStyle.Underline);
		
		// Menu action list
		for (Action action : actions)
		{
			int position = actions.indexOf(action);
			if (position == this.cursorPosition)
			{
				this.screen().putString(this.screenSize().getColumns()-29, 2+position, ">", Terminal.Color.YELLOW, Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold, ScreenCharacterStyle.Blinking);
				this.screen().putString(this.screenSize().getColumns()-27, 2+position, action.message(), Terminal.Color.GREEN, Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold, ScreenCharacterStyle.Blinking);
			} else
			{
				this.screen().putString(this.screenSize().getColumns()-29, 2+position, "|", Terminal.Color.YELLOW, Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold);
				this.screen().putString(this.screenSize().getColumns()-27, 2+position, action.message(), Terminal.Color.WHITE, Terminal.Color.DEFAULT, ScreenCharacterStyle.Bold);
			}
		}
		
		// Player's character stats
		// Footer
		PlayableCharacter pc = Game.instance.mainPlayer();
		String pc_stats = pc.name() + " | HP: " + pc.currentHP() + "/" + pc.maxHP() + " | AP: " + pc.remainingAP() + "/7";
		this.screen().putString(2, this.screenSize().getRows() - 2, pc_stats, Terminal.Color.WHITE, Terminal.Color.BLACK);
		// Side
		this.screen().putString(this.screenSize().getColumns()-28, 20, MessageRepository.messageForId("Level") + ": " + pc.level(), Terminal.Color.CYAN, Terminal.Color.DEFAULT);
		this.screen().putString(this.screenSize().getColumns()-28, 21, MessageRepository.messageForId("Experience") + ": " + pc.experience(), Terminal.Color.CYAN, Terminal.Color.DEFAULT);
		this.screen().putString(this.screenSize().getColumns()-28, 23, MessageRepository.messageForId("Dexterity") + ": " + pc.dexterity(), Terminal.Color.CYAN, Terminal.Color.DEFAULT);
		this.screen().putString(this.screenSize().getColumns()-28, 24, MessageRepository.messageForId("Resistance") + ": " + pc.resistance(), Terminal.Color.CYAN, Terminal.Color.DEFAULT);
		this.screen().putString(this.screenSize().getColumns()-28, 25, MessageRepository.messageForId("Strength") + ": " + pc.strength(), Terminal.Color.CYAN, Terminal.Color.DEFAULT);
		
		// Target footer stats
		if (this.currentMode == Mode.Attack)
		{
			if (actions.get(this.cursorPosition) instanceof AttackCharacterAction)
			{
				Character target = ((AttackCharacterAction)actions.get(this.cursorPosition)).target();
				String target_stats = target.name() + " | HP: " + target.currentHP() + " | AP: " + target.remainingAP() + "/7";
				this.screen().putString(2, this.screenSize().getRows() - 1, target_stats, Terminal.Color.RED, Terminal.Color.BLACK);
			}
		}
	}
	
	private void
	mapRendering()
	{
		// Affichage des tiles
		for (int l = 0; l < Game.instance.mainPlayer().currentMap().layersCount(); l++)
		{
			for (int x = 0; x < Game.instance.mainPlayer().currentMap().size().first(); x++)
			{
				for (int y = 0; y < Game.instance.mainPlayer().currentMap().size().second(); y++)
				{
					Tile t = Game.instance.mainPlayer().currentMap().tileLayer(l).get(new IntPair(x, y));
					if (t == null) continue;
					
					Renderer tr = t.renderer();
					if (tr == null) continue;
					
					t.renderer().renderOnScreen(this.screen(), 2+ x*2, 3+ y);
				}
			}
		}
		
		// Affichage des evenements
		EventLayer evLayer = Game.instance.mainPlayer().currentMap().eventLayer();
		for (int x = 0; x < Game.instance.mainPlayer().currentMap().size().first(); x++)
		{
			for (int y = 0; y < Game.instance.mainPlayer().currentMap().size().second(); y++)
			{
				Event ev = evLayer.get(new IntPair(x, y));
				if (ev != null)
				{
					ev.renderer().renderOnScreen(this.screen(), 2 + 2*x, 3 + y);
				}
			}
		}
		
		// Affichage des personnages
		for (Character character : charactersOnMap())
		{
			character.renderer().renderOnScreen(this.screen(), 2+ character.currentCoords().first().intValue()*2, 3+character.currentCoords().second().intValue());
		}
		
		// Affichage des couts de déplacement si en mode move
		if (this.currentMode == Mode.Move)
		{
			IntPair mp_coords = Game.instance.mainPlayer().currentCoords();
			Map mp_map = Game.instance.mainPlayer().currentMap();
			ArrayList<IntPair> aroundTiles = new ArrayList<IntPair>();
			aroundTiles.add(new IntPair(mp_coords.first().intValue()+1, mp_coords.second().intValue()));
			aroundTiles.add(new IntPair(mp_coords.first().intValue()-1, mp_coords.second().intValue()));
			aroundTiles.add(new IntPair(mp_coords.first().intValue(), mp_coords.second().intValue()+1));
			aroundTiles.add(new IntPair(mp_coords.first().intValue(), mp_coords.second().intValue()-1));
			
			for (IntPair c : aroundTiles)
			{
				if (mp_map.canWalkOnTile(c))
				{
					String cost = "" + (Character.APRequiredToMove + mp_map.tileCost(c));
					while (cost.length() < 2) { cost += " "; }
					
					this.screen().putString(2+c.first()*2, 3+c.second(), cost, Terminal.Color.WHITE, Terminal.Color.CYAN);
				}
			}
		}
		
		//
		this.screen().putString(2, 1, Game.instance.mainPlayer().currentMap().name(), Terminal.Color.WHITE, Terminal.Color.BLACK, ScreenCharacterStyle.Underline);
	}
	
	public void nextTurn() 
	{
		this.cursorPosition = 0;
		Character player = charactersOnMap().get(0);
		
		// Réinsérer le joueur a la fin de la liste des prochains joueurs
		charactersOnMap().remove(player);
		charactersOnMap().add(player);
		
		player.setCurrentAP(7);
		
		if (charactersOnMap().get(0) instanceof PlayableCharacter)
		{
			this.turnCounter++;
			if(turnCounter >=6)
			{
				Game.instance.mainPlayer().currentMap().spawnEnemy();
				turnCounter = 0;
			}
		}
		
		if (charactersOnMap().get(0) instanceof NonPlayableCharacter)
		{
			this.currentMode = Mode.OtherTurn;

		} else
		{
			this.currentMode = Mode.Menu;
		}
	}
	
	@Override
	public void
	processKeyboardEvents(Key k) {
		if (this.currentMode == Mode.Move)
			processKeyBoardEventsForMoveMode(k);
		else {
			switch (k.getKind())
			{
				case ArrowUp:
				{
					this.cursorPosition--;
					if (this.cursorPosition < 0) this.cursorPosition = hudActionList().size()-1;
					break;
				}
				
				case ArrowDown:
				{
					this.cursorPosition++;
					if (this.cursorPosition >= hudActionList().size()) this.cursorPosition = 0;
					break;
				}
				
				case Enter:
				{
					hudActionList().get(this.cursorPosition).execute();
					break;
				}
				
				default:
				{
					break;
				}
			}
		}
	}
	
	private void
	processKeyBoardEventsForMoveMode(Key k) {
		switch (k.getKind())
		{
			case ArrowUp:
			{
				Game.instance.mainPlayer().moveUp();
				break;
			}
			
			case ArrowDown:
			{
				Game.instance.mainPlayer().moveDown();
				break;
			}

			case ArrowLeft:
			{
				Game.instance.mainPlayer().moveLeft();
				break;
			}

			case ArrowRight:
			{
				Game.instance.mainPlayer().moveRight();
				break;
			}

			case Enter:
			{
				hudActionList().get(this.cursorPosition).execute();
				break;
			}

			case Escape:
			{
				this.currentMode = Mode.Menu;
				break;
			}
			
			default:
			{
				break;
			}
		}
	}
	
	public void removeCharacterOnMap(Character c)
	{
		charactersOnMap().remove(c);
	}
	
	public void resetMenu() {
		this.currentMode = Mode.Menu;
		this.cursorPosition = 0;
	}

	@Override
	public void update() {
		this.screen().clear();
		
		this.mapRendering();
		this.hudRendering();
		this.eventMessagesRendering();
		
		if (this.screen() != null)
			this.screen().refresh();
		
		if (this.currentMode == Mode.OtherTurn)
		{			
			((NonPlayableCharacter)charactersOnMap().get(0)).turn();
			nextTurn();
		}
	}

}
