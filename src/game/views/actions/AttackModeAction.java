package game.views.actions;

import game.lang.MessageRepository;
import game.programs.Game;
import game.views.GameView;

public class AttackModeAction extends Action {

	public AttackModeAction() {
		
	}

	@Override
	public void execute() {
		if (Game.instance.currentView() instanceof GameView)
		{
			GameView mv = (GameView)Game.instance.currentView();
			mv.enableAttack();
		}
	}

	@Override
	public String message() {
		return MessageRepository.messageForId("AttackAction");
	}

}
