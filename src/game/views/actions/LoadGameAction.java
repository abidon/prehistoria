package game.views.actions;

import game.lang.MessageRepository;
import game.util.Logger;

public final class LoadGameAction extends Action {
	
	public
	LoadGameAction()
	{
		
	}

	@Override
	public void
	execute()
	{
		Logger.logLine("Unable to load savedata at this time");
	}

	@Override
	public String
	message()
	{
		return MessageRepository.messageForId("loadGameAction");
	}

}
