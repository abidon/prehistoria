package game.views.actions;

import game.lang.MessageRepository;
import game.programs.Game;
import game.util.Logger;

import java.io.FileOutputStream;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class SaveGameAction extends Action {

	public
	SaveGameAction()
	{
		
	}
	
	@SuppressWarnings("static-access")
	@Override
	public void
	execute()
	{
		if (Game.instance.mainPlayer() == null)
		{
			Logger.logDateAndTime().logLine(": Unable to execute action " + this.getClass().getCanonicalName() + " with no mainPlayer");
			return;
		}
		
		// Create the Json Serializer
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().setVersion(1.0).create();
		
		// Make the save data structure
		HashMap<String, Object> saveData = new HashMap<String, Object>();
		saveData.put("playerKind", Game.instance.mainPlayer().getClass().getName());
		saveData.put("player", Game.instance.mainPlayer());
		saveData.put("map", Game.instance.mainPlayer().currentMap().getClass().getName());
		
		try {
			FileOutputStream fstream = new FileOutputStream("data/saves/" + Game.instance.mainPlayer().name() + ".json");
			fstream.write(gson.toJson(saveData).getBytes("UTF-8"));
			fstream.close();
		} catch(Exception e) {}
	}

	@Override
	public String
	message()
	{
		return MessageRepository.messageForId("saveGameAction");
	}

}
