package game.views.actions;

import game.lang.MessageRepository;
import game.programs.Game;
import game.views.GameView;

public class GoBackToParentMenuAction extends Action {

	public GoBackToParentMenuAction() {
		
	}

	@Override
	public void execute() {
		if (Game.instance.currentView() instanceof GameView)
		{
			((GameView)Game.instance.currentView()).resetMenu();
		}
	}

	@Override
	public String message() {
		return MessageRepository.messageForId("GoBack");
	}

}
