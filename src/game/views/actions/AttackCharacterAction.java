package game.views.actions;

import game.characters.Character;

public class AttackCharacterAction extends Action {
	
	private final Character attacker;
	private final Character target;
	
	public AttackCharacterAction(Character attacker, Character target)
	{
		this.attacker = attacker;
		this.target = target;
	}
	
	@Override
	public void
	execute()
	{
		this.attacker.attack(target);
	}
	
	@Override
	public String
	message()
	{
		return this.target.name();
	}
	
	public Character
	target()
	{
		return this.target;
	}
	
}
