package game.views.actions;

import game.environment.Map;
import game.environment.MapTutorial;
import game.programs.Game;
import game.views.GameView;

public final class NewGameAction extends Action {
	
	private final String playerName;
	private final String playerJavaClassPath;
	
	public
	NewGameAction(String playerName, String playerJavaClassPath)
	{
		this.playerName = playerName;
		this.playerJavaClassPath = playerJavaClassPath;
	}
	
	@Override
	public void
	execute() {
		Game.instance.instanciateMainPlayer(playerJavaClassPath, this.playerName);
		Map map = new MapTutorial();
		Game.instance.mainPlayer().setMap(map);
		Game.instance.setView(new GameView());
	}
	
	@Override
	public String
	message()
	{
		return "";
	}
}
