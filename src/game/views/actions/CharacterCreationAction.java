package game.views.actions;

import game.lang.MessageRepository;
import game.programs.Game;
import game.views.CharacterCreationView;

public class CharacterCreationAction extends Action {

	@Override
	public void execute() {
		Game.instance.setView(new CharacterCreationView());
	}

	@Override
	public String message() {
		return MessageRepository.messageForId("newGameAction");
	}

}
