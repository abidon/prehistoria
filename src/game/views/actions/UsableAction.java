package game.views.actions;

import game.items.Usable;
import game.programs.Game;

public class UsableAction extends Action {

	private final Usable u;
	
	public UsableAction(Usable u)
	{
		this.u = u;
	}
	
	@Override
	public void execute() {
//		if (u instanceof Spell)
//		{
//			boolean useOnSelf = ((Spell)u).onSelf();
//			u.use(Game.instance.mainPlayer(), useOnSelf ? Game.instance.mainPlayer() : null);
//		} else {
			u.use(Game.instance.mainPlayer(), Game.instance.mainPlayer());
//		}
	}

	@Override
	public String message() {
		return u.name();
	}

}
