package game.views.actions;

import game.lang.MessageRepository;
import game.programs.Game;
import game.views.MenuView;
import game.views.SureQuitView;

public class BackToMainMenuAction extends Action {
	
	private final boolean sureToQuit;
	
	public BackToMainMenuAction(boolean sureToQuit) {
		this.sureToQuit = sureToQuit;
	}

	@Override
	public void execute() {
		if (this.sureToQuit)
			Game.instance.setView(new SureQuitView(Game.instance.currentView()));
		else Game.instance.setView(new MenuView());
	}

	@Override
	public String message() {
		return MessageRepository.messageForId("backToMainMenuAction");
	}

}
