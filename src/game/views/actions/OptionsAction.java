package game.views.actions;

import game.lang.MessageRepository;
import game.programs.Game;
import game.views.OptionsView;

public final class OptionsAction extends Action {

	public
	OptionsAction()
	{
		
	}
	
	@Override
	public void
	execute()
	{
		Game.instance.setView(new OptionsView());
		//Logger.logDateAndTime().logLine(": Unable to execute action " + this.getClass().getCanonicalName());
	}

	@Override
	public String
	message()
	{
		return MessageRepository.messageForId("optionsAction");
	}

}
