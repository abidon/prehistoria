package game.views.actions;

import game.lang.MessageRepository;
import game.programs.Game;
import game.views.GameView;

public class EndTurnAction extends Action {

	public EndTurnAction() {
		
	}
	
	@Override
	public void execute() {
		if (Game.instance.currentView() instanceof GameView)
		{
			GameView mv = (GameView)Game.instance.currentView();
			mv.nextTurn();
		}
	}

	@Override
	public String message() {
		return MessageRepository.messageForId("EndMyTurn");
	}

}
