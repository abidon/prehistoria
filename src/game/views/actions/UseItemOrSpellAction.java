package game.views.actions;

import game.lang.MessageRepository;
import game.programs.Game;
import game.views.GameView;

public class UseItemOrSpellAction extends Action {

	@Override
	public void execute() {
		if (Game.instance.currentView() instanceof GameView)
		{
			((GameView)Game.instance.currentView()).enableUsable();
		}
	}
	
	@Override
	public String message() {
		return MessageRepository.messageForId("Use");
	}
	
}
