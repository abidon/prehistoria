package game.views.actions;

import game.lang.MessageRepository;
import game.programs.Game;
import game.views.GameView;

public class MoveAction extends Action {

	public MoveAction() {
	}

	@Override
	public void execute() {
		if(Game.instance.currentView() instanceof GameView)
		{
			GameView mv = (GameView)Game.instance.currentView();
			mv.enableMovement();
		}

	}

	@Override
	public String message() {
		return MessageRepository.messageForId("MoveAction");
	}

}
