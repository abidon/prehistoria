package game.views.actions;

public abstract class Action {
	
	public abstract void
	execute();
	
	public abstract String
	message();
	
}
