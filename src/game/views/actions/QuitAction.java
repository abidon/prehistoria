package game.views.actions;

import game.lang.MessageRepository;

public final class QuitAction extends Action {

	public
	QuitAction()
	{
		
	}

	@Override
	public void
	execute() {
		System.exit(0);
	}
	
	@Override
	public String
	message()
	{
		return MessageRepository.messageForId("quitAction");
	}
}
