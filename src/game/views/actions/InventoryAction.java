package game.views.actions;

import game.lang.MessageRepository;
import game.programs.Game;
import game.views.GameView;

public class InventoryAction extends Action {

	public InventoryAction() {
	}

	@Override
	public void execute() {
		if (Game.instance.currentView() instanceof GameView)
		{
			GameView mv = (GameView)Game.instance.currentView();
			mv.enableInventory();
		}
	}

	@Override
	public String message() {
		return MessageRepository.messageForId("InventoryAction");
	}

}
