Prehistoria
===
---

_Prehistoria_ est un __"Survival RPG"__ développé par Aurélien Bidon et Emeric de Peretti dans le cadre de leur études en Informatique à l'IUT de Villetaneuse.  

---

#### Background

Le jeu prend place dans un **univers préhistorique** dans lequel le joueur doit **lutter pour sa survie** face a une **nature meurtrière**. Le **craft** et l'**évolution** prennent une place importante dans le jeu puisque la survie de l'Homme a, à l'époque, reposé sur la **découverte de technologies** aujourd'hui évidentes telles que le feu, la fabrication d'outils/armes, la confection de vetements.  
Une dimension fictive sera ajoutée au jeu a travers l'apprentissage de **sorts**, la confection d'**armes éthérées** (*fleche de feu*, etc…) qui appliqueront un **effet** au joueur ennemi (ex: *brulé, empoisonné, gelé, reduit au silence*, etc…).

---

#### Features

L'ensemble des possibilités offertes par le jeu reste à définir, l'ensemble des idées ne pouvant être implémentées par une équipe aussi réduite. La liste (non exhaustive) prendra place dans ce document au fur et à mesure que le projet avance.

---

#### Useful links

* [Site sur les jeux en très global (pleins d'algorithmes terrain, IA, etc...)](http://www-cs-students.stanford.edu/~amitp/gameprog.html)

* [Probabilitées et dommages dans un RPG](http://www.redblobgames.com/articles/probability/damage-rolls.html)

---

#### Informations sur le développement

* Taille maximale des maps: 44*24